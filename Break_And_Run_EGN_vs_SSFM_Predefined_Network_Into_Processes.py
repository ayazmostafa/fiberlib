# -*- coding: utf-8 -*-
"""
Created on Tue Dec 27 20:19:55 2022

@author: Mostafa
"""
# from subprocess import Popen,PIPE
# from asyncio import subprocess
from RWALib import Elastic_RWA,importPhysicalTopology,importTrafficMatrix
# from json import loads,dump
# from numpy.random import shuffle
# from os import mkdir
import subprocess
from TopologyAndTrafficManager import createRandomHash
#%%
if __name__=="__main__":
    
    ##########################################################################
    ##########################################################################
    #########################                       ##########################
    ######################### Adjustable parameters ##########################
    #########################                       ##########################
    ##########################################################################
    ##########################################################################
    # Topology and traffic information
    PT_TM_import_info=[
        # ("Backbone_Data","Test1_PT","Test1_TM"),
        # ("Backbone_Data","US_PhysicalTopology","US_Total_TrafficMatrix"),
        ("Backbone_Data","EU_PhysicalTopology","EU_Total_TrafficMatrix"),
        # ("Backbone_Data","GER_PhysicalTopology","GER_Total_TrafficMatrix"),
        ]
    
    # US chosen LOI IDs (Lightpath link number >= 4)
    # chosen_loi_ids=['dem3', 'dem5', 'dem10', 'dem18', 'dem20', 'dem22', 'dem29', 'dem34', 'dem35',
    #     'dem40', 'dem42','dem46', 'dem52', 'dem66', 'dem67', 'dem85', 'dem89', 'dem93', 'dem95', 'dem98',
    #     'dem104', 'dem107','dem119', 'dem120', 'dem131', 'dem142', 'dem150', 'dem167', 'dem173', 'dem177']
     
    # # EU chosen LOI IDs (Lightpath link number >= 8)
    chosen_loi_ids=['dem57', 'dem72', 'dem76', 'dem79', 'dem84', 'dem86', 'dem94', 'dem96', 'dem139', 'dem157',
        'dem160', 'dem204', 'dem355', 'dem373', 'dem376', 'dem400', 'dem409', 'dem413', 'dem423', 'dem427',
        'dem430', 'dem454', 'dem489', 'dem502', 'dem597', 'dem600', 'dem608', 'dem609', 'dem610', 'dem611',
        'dem678', 'dem681', 'dem689', 'dem691']
    
    # # GER chosen LOI IDs (Lightpath link number >= 5)
    # chosen_loi_ids=['dem29', 'dem30', 'dem44', 'dem45', 'dem59', 'dem60', 'dem68', 'dem72', 'dem74', 'dem75',
    #     'dem105','dem106', 'dem121', 'dem126', 'dem151', 'dem186', 'dem214', 'dem215', 'dem216', 'dem217',
    #     'dem219','dem229', 'dem230', 'dem231', 'dem232', 'dem234', 'dem235', 'dem237']
    ##########################################################################
    ##########################################################################
    #####################                              #######################
    ##################### End of adjustable parameters #######################
    #####################                              #######################
    ##########################################################################
    ##########################################################################
    processes = []
    process_ids = []
    args_list=[]
    
    for PT_TM_DB_dir,PT_title,TM_title in PT_TM_import_info:
        
        nodes,linkLengths=importPhysicalTopology(PT_title,PT_TM_DB_dir)
        unnormalizedTrafficMatrix=importTrafficMatrix(TM_title,PT_TM_DB_dir)
        lpdict=Elastic_RWA(nodes,linkLengths,unnormalizedTrafficMatrix)[0]
        
        # chosen_loi_ids=list(lpdict.copy().keys())
        # for _ in range(len(lpdict)-random_number_of_loi_ids):
        #     shuffle(chosen_loi_ids)
        #     chosen_loi_ids.pop(0)
        
        # raise Exception("sadsadsad")
        # chosen_loi_ids=["dem47"]*10
        # processes = []
        SNR_script = 'EGN_vs_SSFM_Predefined_Network.py'
        print("\fProcesses started.")

        for loi_id in chosen_loi_ids:
            process_id=createRandomHash(hashlength=20)
            # args_list.append({
            #     # "power_range_db":power_range_db,
            #     # "rrc_beta":rrc_beta,
            #     # "nuMCp":nuMCp,
            #     # "numstep":numstep,
            #     # "n_sym":n_sym,
            #     # "upsratio_elevation":upsratio_elevation,
            #     # "sym_del_margin_ratio":sym_del_margin_ratio,
            #     # "Fs_type":Fs_type,
            #     "process_id": process_id,
            #     "PT_TM_DB_dir":PT_TM_DB_dir,
            #     "PT_title":PT_title,
            #     "TM_title":TM_title,
            #     "loi_id":loi_id,
            #     # "modulationType":modulationType,
            #     # "nuOffset":nuOffset,
            #     # "slotBandwidth_GHz":slotBandwidth_GHz,
            #     # "numOfGuardSlotsPerDemand":numOfGuardSlotsPerDemand,
            #     # "span_approx_length_km":span_approx_length_km,
            #     # "ampgain":ampgain,
            #     # "fiber_type":fiber_type,
            #     # "nf":nf,
            #     # "results_parent_dir":results_parent_dir,
            #     # "progress_bar_position": chosen_loi_ids.index(loi_id),
            #     # "is_progress_bar_enabled": is_progress_bar_enabled,
            #     })
            processes.append(subprocess.Popen(["python",SNR_script,process_id,PT_TM_DB_dir,PT_title,TM_title,loi_id]))
            process_ids.append(process_id)

    exit_codes = [process.wait() for process in processes]
    
    for process_id,exit_code in zip(process_ids,exit_codes):
        if not exit_code==0:
            print(f"Process {process_id} exited with error.")
    
    if not any(exit_codes):
        print("No errors occured.")
    
    # results=[process.stdout.read().decode().strip().replace("'","\"") for process in processes]
    
    # results=[loads(process.stdout.read().decode().strip().replace("'","\"")) for process in processes]
    
    #%%
    # for single_result,exit_code in zip(results,exit_codes):
        
    #     if not single_result["err"]=="":
    #         print(f"ERROR: {single_result['err']}")
    #         continue
        
    #     single_result_dir=single_result["result_dir"]
        
    #     if not exit_code==0:
    #         print("Flawed result detected. Marking flaw on saved data.")
    #         single_result_dir="[ERROR_ON_EXIT] "+single_result_dir
        
    #     try:
    #         mkdir(single_result["results_parent_dir"])
    #     except FileExistsError:
    #         pass
    #     # [print(f'python3 EGN_vs_SSFM_Predefined_Network.py "{args}"',end="\n\n") for args in args_list]
    #     # raise Exception("Hi----------------")

    #     mkdir(single_result_dir)
        
    #     for data_name in ['inputs', 'SNR_info', 'SCI_XCI_MCI_info']:
    #         dump(single_result[data_name],open(single_result_dir+"/"+f"{data_name}.json","w+"))
        
    #     if not exit_code==0:
    #         dump(str(single_result["err"]),open(single_result_dir+"/"+"error_log.json","w+"))