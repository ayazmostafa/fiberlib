# -*- coding: utf-8 -*-
"""
Created on Sat Apr  9 05:04:57 2022

@author: M
"""

# import matplotlib.pyplot as plt
from numpy import pi,exp,real,log10,mean,array,isnan,cumsum
# from SSFM import lpdictplot
# from TopologyAndTrafficManager import randomSimulation,setUpSpanPropertiesAccordingToTheLOI
from numpy.random import uniform
# from tqdm import tqdm
from numpy.linalg import norm
from json import load as jsonload
# import numpy as np
# from os import mkdir
# from RWALib import importPhysicalTopology,importTrafficMatrix,Elastic_RWA
#%%
def _2DMC_(func,x0,x1,y0,y1,nuMCp=1e6):
    nuMCp=int(nuMCp)
    x=uniform(x0,x1,int(nuMCp))
    y=uniform(y0,y1,int(nuMCp))
    return mean(func(x,y))*(x1-x0)*(y1-y0)
#%%
def _3DMC_(func,x0,x1,y0,y1,z0,z1,nuMCp=1e6):
    nuMCp=int(nuMCp)
    x=uniform(x0,x1,nuMCp)
    y=uniform(y0,y1,nuMCp)
    z=uniform(z0,z1,nuMCp)
    return mean(func(x,y,z))*(x1-x0)*(y1-y0)*(z1-z0)
#%%
def _4DMC_(func,w0,w1,x0,x1,y0,y1,z0,z1,nuMCp=1e6):
    nuMCp=int(nuMCp)
    w=uniform(w0,w1,nuMCp)
    x=uniform(x0,x1,nuMCp)
    y=uniform(y0,y1,nuMCp)
    z=uniform(z0,z1,nuMCp)
    return mean(func(w,x,y,z))*(w1-w0)*(x1-x0)*(y1-y0)*(z1-z0)
#%%
def _5DMC_(func,v0,v1,w0,w1,x0,x1,y0,y1,z0,z1,nuMCp=1e6):
    nuMCp=int(nuMCp)
    v=uniform(v0,v1,nuMCp)
    w=uniform(w0,w1,nuMCp)
    x=uniform(x0,x1,nuMCp)
    y=uniform(y0,y1,nuMCp)
    z=uniform(z0,z1,nuMCp)
    return mean(func(v,w,x,y,z))*(v1-v0)*(w1-w0)*(x1-x0)*(y1-y0)*(z1-z0)
#%%
def S_RRC(f,symbolrate):
    '''The pulse shape implemented in this function is ideal sinc.'''
    return (abs(f)<symbolrate/2)/symbolrate
#%%
def preProcessing(lpdict,loi_id,alpha_dict,beta2_dict,gamma_dict,
                        lspan_dict,ampgain_dict,nf_dict,launchpower_dict):
    '''Converts links to spans.'''
    alpha_list=[]
    beta2_list=[]
    gamma_list=[]
    lspan_list=[]
    ampgain_list=[]
    nf_list=[]
    pair_list=[]
    
    valid_pairs_per_model_types_dict={
        "Ex. EGN" :set(),
        "iEGN"    :set(),
        "iGN"     :set(),
        }
    
    num_spans_per_links_list=[]
    
    loi_node_list=lpdict[loi_id]["nodeList"]
    loi_link_list=list(zip(loi_node_list,loi_node_list[1:]))
    
    for i,j in loi_link_list:
        alpha_list+=alpha_dict[i,j]
        beta2_list+=beta2_dict[i,j]
        gamma_list+=gamma_dict[i,j]
        lspan_list+=lspan_dict[i,j]
        ampgain_list+=ampgain_dict[i,j]
        nf_list+=nf_dict[i,j]
        
        link_nspan=len(alpha_dict[i,j])
        
        link_lpid_set=set()
        for lpid,lp in lpdict.items():
            if (i,j) in zip(lp['nodeList'],lp['nodeList'][1:]):
                link_lpid_set.add(lpid)
        
        pair_list+=[link_lpid_set]*link_nspan
        
        num_spans_per_links_list.append(len(alpha_dict[i,j]))
    
    num_spans_per_links_expanded=[list(range(i,j)) for i,j in zip(cumsum([0]+num_spans_per_links_list),cumsum([0]+num_spans_per_links_list)[1:])]
    
    for k_range_index,k_range in enumerate(num_spans_per_links_expanded):
        for kp_range_index,kp_range in enumerate(num_spans_per_links_expanded):
            if kp_range_index<k_range_index:
                continue
            for k in k_range:
                for kp in kp_range:
                    if kp<k:
                        continue
                    valid_pairs_per_model_types_dict["Ex. EGN"].add((k,kp))
                    if k_range_index==kp_range_index:
                        # When k_range_index is equal to kp_range_index, we are iterating over the same link.
                        # In this case, we can inject the entry under the "iegn" key.
                        valid_pairs_per_model_types_dict["iEGN"].add((k,kp))
                        # When k is equal to kp, the spans are equal either and the entry is also injected
                        # under the "ign" key.
                        if k==kp:
                            valid_pairs_per_model_types_dict["iGN"].add((k,kp))
    
    """Creating databases required for GN and EGN models calculations"""
    ATTGAINDICT={}
    BETA2DICT={}
    numspan=len(alpha_list)
    for c1 in range(1,1+1+numspan):
        ATTGAINDICT[c1,c1-1]=1
        BETA2DICT[c1,c1-1]=0
        for c2 in range(c1,1+numspan):
            ATTGAINDICT[c1,c2]=ATTGAINDICT[c1,c2-1]*\
            exp(-alpha_list[c2-1]*lspan_list[c2-1]/2)*ampgain_list[c2-1]**0.5
            BETA2DICT[c1,c2]=BETA2DICT[c1,c2-1]-\
            2j*pi**2*beta2_list[c2-1]*lspan_list[c2-1]
    
    """Lightpath powers at the bus intersection and first node intersections"""
    
    
    # raise Exception("Fix this paaaaaart!!!!!!!!")
    
    power_at_bus_intersection_dict={}
    first_common_node_dict={}
    
    for lpid,lp in lpdict.items():
        
        lpnodelist=lp['nodeList']
        lplinklist=list(zip(lpnodelist,lpnodelist[1:]))
        
        lp_1st_common_node=None
        
        for node in lpnodelist:
            if node in loi_node_list:
                lp_1st_common_node=node
                break
        
        if lp_1st_common_node==None:
            continue
        # lp_1st_common_node=list(filter(lambda node: node in loi_node_list,lpnodelist))[0]
        first_common_node_dict[lpid]=1+loi_node_list.index(lp_1st_common_node)
        
        lp_power_at_bus_intersection=launchpower_dict[lpid]
        for p,q in lplinklist:
            link_alpha_list=alpha_dict[p,q]
            link_lspan_list=lspan_dict[p,q]
            link_ampgain_list=ampgain_dict[p,q]
            for span_alpha,span_length,span_amp_gain in zip(link_alpha_list,link_lspan_list,link_ampgain_list):
                lp_power_at_bus_intersection*=exp(-span_alpha*span_length)*span_amp_gain
        
        power_at_bus_intersection_dict[lpid]=lp_power_at_bus_intersection
    
    """Phi and Psi for modulation formats"""
    Phi_dict={}
    Psi_dict={}
    
    all_modulation_formats=jsonload(open("Modulation_Alphabets_4D.json","r"))
    
    for lpid,lp in lpdict.items():
        modulation_alphabet=all_modulation_formats[lpdict[lpid]['modulationType']]
        modulation_phi=\
        mean(norm(modulation_alphabet,2,axis=1)**4)/\
        mean(norm(modulation_alphabet,2,axis=1)**2)**2-2
        modulation_psi=\
        mean(norm(modulation_alphabet,2,axis=1)**6)/\
        mean(norm(modulation_alphabet,2,axis=1)**2)**3-9*modulation_phi-6
        Phi_dict[lpid]=modulation_phi
        Psi_dict[lpid]=modulation_psi
    
    return alpha_list,beta2_list,gamma_list,lspan_list,ampgain_list,nf_list,\
        pair_list,power_at_bus_intersection_dict,first_common_node_dict,\
        Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,valid_pairs_per_model_types_dict
#%%
def psi(f1,f2,f,alpha,beta2,gamma,lspan):
    if alpha<1e-7:
        raise Exception(f'Low attenuation (alpha = {alpha}).')
    arg=alpha-4j*pi**2*beta2*(f1-f)*(f2-f)
    return gamma*(1-exp(-arg*lspan))/arg
#%%
def DblUpsilon(f1,f2,f3,f4,f,k,kp,i1,i2,i3,ATTGAINDICT,BETA2DICT,
               alpha_list,beta2_list,gamma_list,lspan_list):
    """This function implements the double product of the Upsilon functions."""
    
    Ns=len(alpha_list)
    
    """For simplicity :)"""
    A=ATTGAINDICT
    B=BETA2DICT
    
    alpha_k=alpha_list[k-1]
    beta2_k=beta2_list[k-1]
    gamma_k=gamma_list[k-1]
    lspan_k=lspan_list[k-1]
    alpha_kp=alpha_list[kp-1]
    beta2_kp=beta2_list[kp-1]
    gamma_kp=gamma_list[kp-1]
    lspan_kp=lspan_list[kp-1]
    
    psi_k=psi(f1,f2,f,alpha_k,beta2_k,gamma_k,lspan_k)
    psi_kp=psi(f1,f2,f,alpha_kp,beta2_kp,gamma_kp,lspan_kp)
    
    att_prods=A[i1,k-1]*A[i2,k-1]*A[i3,k-1]*A[k,Ns]*\
        A[i1,kp-1]*A[i2,kp-1]*A[i3,kp-1]*A[kp,Ns]
    
    exp_sums=B[i1,k-1]*f1**2+B[i2,k-1]*f2**2-B[i3,k-1]*(f1+f2-f)**2+B[k,Ns]*f**2-\
        B[i1,kp-1]*f3**2-B[i2,kp-1]*f4**2+B[i3,kp-1]*(f3+f4-f)**2-B[kp,Ns]*f**2
        
    # print(psi_kp)
    
    return psi_k*psi_kp.conj()*att_prods*exp(exp_sums)
#%%
def EGN_NLI_terms(lpdict,loi_id,alpha_list,beta2_list,gamma_list,lspan_list,
    first_common_node_dict,pair_list,Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,
    valid_pairs_per_model_types_dict,included_models=["iEGN"],nuMCp=1e6,
    progress_bar_position=0,is_progress_bar_enabled=1,result_dir=""):
    
    max_loi_ch_freq=lpdict[loi_id]["nu"]+lpdict[loi_id]["chBandwidth"]/2
    min_loi_ch_freq=lpdict[loi_id]["nu"]-lpdict[loi_id]["chBandwidth"]/2
    
    included_tuples=set()
    for model_type in included_models:
        included_tuples|=valid_pairs_per_model_types_dict[model_type]
    
    NLI_terms            = {}
    # NLI_terms["iGN"]     = {}
    NLI_terms["iEGN"]    = {}
    # NLI_terms["Ex. EGN"] = {}
    
    # NLI_terms["iGN (SCI)"]     = {}
    NLI_terms["iEGN (SCI)"]    = {}
    # NLI_terms["Ex. EGN (SCI)"] = {}
    
    # NLI_terms["iGN (XCI+MCI)"]     = {}
    NLI_terms["iEGN (XCI+MCI)"]    = {}
    # NLI_terms["Ex. EGN (XCI+MCI)"] = {}
    
    # print(f"is_progress_bar_enabled={is_progress_bar_enabled}")
    if is_progress_bar_enabled==1:
        total_progress=len(included_tuples)
        # for k in range(1,1+Ns):
        #     for kp in range(k,1+Ns):
        #         Lambda_k=pair_list[k-1]
        #         Lambda_kp=pair_list[kp-1]
        #         Lambda_k_kp=Lambda_k & Lambda_kp
        #         total_progress+=len(Lambda_k & Lambda_kp)**3
        # progressbar=tqdm(total=total_progress,leave=False,position=progress_bar_position)
    
    current_progress=0
    
    # for k in range(1,1+Ns):
    #     for kp in range(k,1+Ns):
            
    #         if model_type in ["iGN","iEGN"] and not k==kp:
    #             continue
    for k1,kp1 in included_tuples:
        
        k=k1+1
        kp=kp1+1
        
        # NLI_terms["Ex. EGN"]           [k,kp] = {}
        # NLI_terms["Ex. EGN (SCI)"]     [k,kp] = {}
        # NLI_terms["Ex. EGN (XCI+MCI)"] [k,kp] = {}
        # NLI_terms["iGN"]               [k,kp] = {}
        # NLI_terms["iGN (SCI)"]         [k,kp] = {}
        # NLI_terms["iGN (XCI+MCI)"]     [k,kp] = {}
        NLI_terms["iEGN"]              [k,kp] = {}
        NLI_terms["iEGN (SCI)"]        [k,kp] = {}
        NLI_terms["iEGN (XCI+MCI)"]    [k,kp] = {}
        
        Lambda_k_kp=pair_list[k-1] & pair_list[kp-1]
        
        for lpid1 in Lambda_k_kp:
            for lpid2 in Lambda_k_kp:
                for lpid3 in Lambda_k_kp:
                    
                    lpid1_nu = lpdict[lpid1]['nu']
                    lpid2_nu = lpdict[lpid2]['nu']
                    lpid3_nu = lpdict[lpid3]['nu']
                    Omega    = lpid1_nu+lpid2_nu-lpid3_nu
                    
                    i1       = first_common_node_dict[lpid1]
                    i2       = first_common_node_dict[lpid2]
                    i3       = first_common_node_dict[lpid3]
                    
                    sr1      = lpdict[lpid1]['symbolRate']
                    sr2      = lpdict[lpid2]['symbolRate']
                    sr3      = lpdict[lpid3]['symbolRate']
                    
                    dblUpsilon_for_D_term=lambda f1,f2,f: DblUpsilon(
                        f1+lpid1_nu,f2+lpid2_nu,f1+lpid1_nu,f2+lpid2_nu,f,
                        k,kp,i1,i2,i3,ATTGAINDICT,BETA2DICT,
                        alpha_list,beta2_list,gamma_list,lspan_list)
                    D_term=16/27*sr1*sr2*sr3*_3DMC_(
                        lambda f1,f2,f: dblUpsilon_for_D_term(f1,f2,f)*\
                        abs(S_RRC(f1,sr1))**2*abs(S_RRC(f2,sr2))**2*\
                        abs(S_RRC(f1+f2-f+Omega,sr3))**2,
                        -sr1/2,sr1/2,-sr2/2,sr2/2,min_loi_ch_freq,max_loi_ch_freq,nuMCp)
                    
                    E_term=0
                    F_term=0
                    G_term=0
                    
                    if not included_models==["iGN"]:
                    
                        if lpid2==lpid3:
                            dblUpsilon_for_E_term=lambda f1,f2,f2p,f: DblUpsilon(
                            f1+lpid1_nu,f2+lpid2_nu,f1+lpid1_nu,f2p+lpid2_nu,f,
                            k,kp,i1,i2,i3,ATTGAINDICT,BETA2DICT,
                            alpha_list,beta2_list,gamma_list,lspan_list)
                            E_term=80/81*sr1*sr2*Phi_dict[lpid2]*_4DMC_(
                                lambda f1,f2,f2p,f: dblUpsilon_for_E_term(f1,f2,f2p,f)*\
                                abs(S_RRC(f1,sr1))**2*S_RRC(f2,sr2).conj()*S_RRC(f2p,sr2).conj()*\
                                S_RRC(f1+f2-f+Omega,sr2).conj()*S_RRC(f1+f2p-f+Omega,sr2),
                                -sr1/2,sr1/2,-sr2/2,sr2/2,-sr2/2,sr2/2,
                                min_loi_ch_freq,max_loi_ch_freq,nuMCp)
                            
                        if lpid1==lpid2:
                            dblUpsilon_for_F_term=lambda f1,f2,f1p,f: DblUpsilon(
                            f1+lpid1_nu,f2+lpid2_nu,f1p+lpid1_nu,f1+f2-f1p+lpid2_nu,f,
                            k,kp,i1,i2,i3,ATTGAINDICT,BETA2DICT,
                            alpha_list,beta2_list,gamma_list,lspan_list)
                            F_term=16/81*sr1*sr3*Phi_dict[lpid1]*_4DMC_(
                                lambda f1,f2,f1p,f: dblUpsilon_for_F_term(f1,f2,f1p,f)*\
                                abs(S_RRC(f1+f2-f+Omega,sr3))**2*S_RRC(f1,sr1)*S_RRC(f2,sr1)*\
                                S_RRC(f1p,sr1).conj()*S_RRC(f1+f2-f1p,sr1).conj(),
                                -sr1/2,sr1/2,-sr1/2,sr1/2,-sr1/2,sr1/2,
                                min_loi_ch_freq,max_loi_ch_freq,nuMCp)
                            
                        if lpid1==lpid2==lpid3:
                            dblUpsilon_for_G_term=lambda f1,f2,f1p,f2p,f: DblUpsilon(
                            f1p+lpid1_nu,f2+lpid2_nu,f1p+lpid1_nu,f2p+lpid2_nu,f,
                            k,kp,i1,i2,i3,ATTGAINDICT,BETA2DICT,
                            alpha_list,beta2_list,gamma_list,lspan_list)
                            G_term=16/81*sr1*Psi_dict[lpid1]*_5DMC_(
                                lambda f1,f2,f1p,f2p,f: dblUpsilon_for_G_term(f1,f2,f1p,f2p,f)*\
                                S_RRC(f1,sr1)*S_RRC(f2,sr1)*S_RRC(f1p,sr1).conj()*S_RRC(f2p,sr1).conj()*\
                                S_RRC(f1+f2-f+Omega,sr1).conj()*S_RRC(f1p+f2p-f+Omega,sr1),
                                -sr1/2,sr1/2,-sr1/2,sr1/2,-sr1/2,sr1/2,-sr1/2,sr1/2,
                                min_loi_ch_freq,max_loi_ch_freq,nuMCp)
                    
                    # if "Ex. EGN" in included_models and (k1,kp1) in valid_pairs_per_model_types_dict["Ex. EGN"]:
                    #     NLI_terms["Ex. EGN"][k,kp][lpid1,lpid2,lpid3,loi_id]=\
                    #         real(D_term+E_term+F_term+G_term)*(1+(k!=kp))
                    #     if lpid1==lpid2==lpid3==loi_id:
                    #         NLI_terms["Ex. EGN (SCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=\
                    #             real(D_term+E_term+F_term+G_term)*(1+(k!=kp))
                    #     else:
                    #         NLI_terms["Ex. EGN (XCI+MCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=\
                    #             real(D_term+E_term+F_term+G_term)*(1+(k!=kp))
                    
                    if "iEGN" in included_models and (k1,kp1) in valid_pairs_per_model_types_dict["iEGN"]:
                        NLI_terms["iEGN"][k,kp][lpid1,lpid2,lpid3,loi_id]=\
                            real(D_term+E_term+F_term+G_term)*(1+(k!=kp))
                        if lpid1==lpid2==lpid3==loi_id:
                            NLI_terms["iEGN (SCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=\
                                real(D_term+E_term+F_term+G_term)*(1+(k!=kp))
                        else:
                            NLI_terms["iEGN (XCI+MCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=\
                                real(D_term+E_term+F_term+G_term)*(1+(k!=kp))
                    
                    if "iGN" in included_models and (k1,kp1) in valid_pairs_per_model_types_dict["iGN"]:
                        NLI_terms["iGN"][k,kp][lpid1,lpid2,lpid3,loi_id]=real(D_term)
                        if lpid1==lpid2==lpid3==loi_id:
                            NLI_terms["iGN (SCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=real(D_term)
                        else:
                            NLI_terms["iGN (XCI+MCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=real(D_term)
                        
                        # if model_type in ["general","iEGN"]:
                            
                        #     NLI_terms["iEGN"][k,kp][lpid1,lpid2,lpid3,loi_id]=real(D_term+E_term+F_term+G_term)
                        #     if lpid1==lpid2==lpid3==loi_id:
                        #         NLI_terms["iEGN (SCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=real(D_term+E_term+F_term+G_term)
                        #     else:
                        #         NLI_terms["iEGN (XCI+MCI)"][k,kp][lpid1,lpid2,lpid3,loi_id]=real(D_term+E_term+F_term+G_term)
                    
        current_progress+=1
        if is_progress_bar_enabled==1:
            with open(result_dir+"/NLI_modeling_progress_log.txt","a") as file:
                file.writelines(f"{current_progress} of {total_progress} completed.\n")
                        # progressbar.update(1)
    
    return NLI_terms
#%%
def SNR(lpdict,NLI_terms,ampgain_list,nf_list,loi_id,power_at_bus_intersection_dict,ATTGAINDICT):
    
    """ASE"""
    h_planck=6.626e-34
    nu_opt=3e8/1550e-9
    ASE_variance=0
    for n in range(1,1+len(nf_list)):
        ASE_variance+=h_planck*nu_opt*ampgain_list[n-1]*nf_list[n-1]*\
            lpdict[loi_id]["symbolRate"]*ATTGAINDICT[n+1,len(nf_list)]
    
    """NLI"""
    NLI_variance=0
    for k,kp in NLI_terms:
        for lpid1,lpid2,lpid3,lpid4 in NLI_terms[k,kp]:
            # If the lpid does not belong the the lpdict, the iteration should be skipped.
            if not (lpid1 in lpdict and lpid2 in lpdict and lpid3 in lpdict and lpid4 in lpdict):
                continue
            if not lpid4==loi_id:
                continue
            P1=power_at_bus_intersection_dict[lpid1]
            P2=power_at_bus_intersection_dict[lpid2]
            P3=power_at_bus_intersection_dict[lpid3]
            NLI_variance+=NLI_terms[k,kp][lpid1,lpid2,lpid3,lpid4]*P1*P2*P3
    
    loi_snr=power_at_bus_intersection_dict[loi_id]/(ASE_variance+NLI_variance)
    
    return 10*log10(loi_snr),NLI_variance,ASE_variance
#%%
def SNR_per_num_side_channels(lpdict,model_lpdictdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
        ampgain_dict,nf_dict,power_range_db,nuMCp,progress_bar_position,is_progress_bar_enabled,result_dir):
    
    alpha_list,beta2_list,gamma_list,lspan_list,ampgain_list,nf_list,\
    pair_list,power_at_bus_intersection_dict,first_common_node_dict,Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,\
    valid_pairs_per_model_types_dict=preProcessing(lpdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
                       ampgain_dict,nf_dict,{lpid: 1 for lpid in lpdict})
    
    NLI_terms=EGN_NLI_terms(lpdict,loi_id,alpha_list,beta2_list,gamma_list,lspan_list,
        first_common_node_dict,pair_list,Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,
        valid_pairs_per_model_types_dict,included_models=["iEGN"],nuMCp=nuMCp,
        progress_bar_position=progress_bar_position,is_progress_bar_enabled=is_progress_bar_enabled,
        result_dir=result_dir)
    
    _,NLI_eta,ASE_variance=SNR(lpdict,NLI_terms["iEGN"],ampgain_list,
            nf_list,loi_id,power_at_bus_intersection_dict,ATTGAINDICT)
    
    p_opt_dbm=10*log10(ASE_variance/2/NLI_eta)/3+30
    if p_opt_dbm>1e10:
        p_opt_dbm=0
    if isnan(p_opt_dbm):
        print("[WARNING] p_opt_dbm is NaN and hence, was set to 0.")
        p_opt_dbm=0
    power_set_dbm=array(power_range_db)+p_opt_dbm
    
    exegn_snr_db_list_dict={}
    iegn_snr_db_list_dict={}
    ign_snr_db_list_dict={}
    
    exegn_sci_list_dict={}
    iegn_sci_list_dict={}
    ign_sci_list_dict={}
    
    exegn_xci_mci_list_dict={}
    iegn_xci_mci_list_dict={}
    ign_xci_mci_list_dict={}
    
    for max_channel_index_from_aside,model_lpdict in model_lpdictdict.items():
        
        temp_exegn_snr_db_list=[]
        temp_iegn_snr_db_list=[]
        temp_ign_snr_db_list=[]
        
        temp_exegn_sci_list=[]
        temp_iegn_sci_list=[]
        temp_ign_sci_list=[]
        
        temp_exegn_xci_mci_list=[]
        temp_iegn_xci_mci_list=[]
        temp_ign_xci_mci_list=[]
        
        for power_dbm in power_set_dbm:
            
            launchpower_dict={lpid:10**(0.1*power_dbm-3) for lpid in model_lpdict}
            
            alpha_list,beta2_list,gamma_list,lspan_list,ampgain_list,nf_list,\
            pair_list,power_at_bus_intersection_dict,first_common_node_dict,\
            Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,_=preProcessing(model_lpdict,loi_id,alpha_dict,
                beta2_dict,gamma_dict,lspan_dict,ampgain_dict,nf_dict,launchpower_dict)
            
            # exegn_snr_db,_,__=SNR(model_lpdict,NLI_terms["Ex. EGN"],ampgain_list,nf_list,loi_id,
            #             power_at_bus_intersection_dict,ATTGAINDICT)
            iegn_snr_db,_,__=SNR(model_lpdict,NLI_terms["iEGN"],ampgain_list,nf_list,loi_id,
                        power_at_bus_intersection_dict,ATTGAINDICT)
            # ign_snr_db,_,__=SNR(model_lpdict,NLI_terms["iGN"],ampgain_list,nf_list,loi_id,
            #             power_at_bus_intersection_dict,ATTGAINDICT)
            
            # _,exegn_sci,__=SNR(model_lpdict,NLI_terms["Ex. EGN (SCI)"],ampgain_list,nf_list,loi_id,
            #             power_at_bus_intersection_dict,ATTGAINDICT)
            _,iegn_sci,__=SNR(model_lpdict,NLI_terms["iEGN (SCI)"],ampgain_list,nf_list,loi_id,
                        power_at_bus_intersection_dict,ATTGAINDICT)
            # _,ign_sci,__=SNR(model_lpdict,NLI_terms["iGN (SCI)"],ampgain_list,nf_list,loi_id,
            #             power_at_bus_intersection_dict,ATTGAINDICT)
            
            # _,exegn_xci_mci,__=SNR(model_lpdict,NLI_terms["Ex. EGN (XCI+MCI)"],ampgain_list,nf_list,loi_id,
            #             power_at_bus_intersection_dict,ATTGAINDICT)
            _,iegn_xci_mci,__=SNR(model_lpdict,NLI_terms["iEGN (XCI+MCI)"],ampgain_list,nf_list,loi_id,
                        power_at_bus_intersection_dict,ATTGAINDICT)
            # _,ign_xci_mci,__=SNR(model_lpdict,NLI_terms["iGN (XCI+MCI)"],ampgain_list,nf_list,loi_id,
            #             power_at_bus_intersection_dict,ATTGAINDICT)
            
            # temp_exegn_snr_db_list.append(exegn_snr_db)
            temp_iegn_snr_db_list.append(iegn_snr_db)
            # temp_ign_snr_db_list.append(ign_snr_db)
            
            # temp_exegn_sci_list.append(exegn_sci)
            temp_iegn_sci_list.append(iegn_sci)
            # temp_ign_sci_list.append(ign_sci)
            
            # temp_exegn_xci_mci_list.append(exegn_xci_mci)
            temp_iegn_xci_mci_list.append(iegn_xci_mci)
            # temp_ign_xci_mci_list.append(ign_xci_mci)
        
        exegn_snr_db_list_dict[max_channel_index_from_aside]=temp_exegn_snr_db_list
        iegn_snr_db_list_dict[max_channel_index_from_aside]=temp_iegn_snr_db_list
        ign_snr_db_list_dict[max_channel_index_from_aside]=temp_ign_snr_db_list
        
        exegn_sci_list_dict[max_channel_index_from_aside]=temp_exegn_sci_list
        iegn_sci_list_dict[max_channel_index_from_aside]=temp_iegn_sci_list
        ign_sci_list_dict[max_channel_index_from_aside]=temp_ign_sci_list
        
        exegn_xci_mci_list_dict[max_channel_index_from_aside]=temp_exegn_xci_mci_list
        iegn_xci_mci_list_dict[max_channel_index_from_aside]=temp_iegn_xci_mci_list
        ign_xci_mci_list_dict[max_channel_index_from_aside]=temp_ign_xci_mci_list
    
    return power_set_dbm,exegn_snr_db_list_dict,iegn_snr_db_list_dict,ign_snr_db_list_dict,\
        exegn_sci_list_dict,iegn_sci_list_dict,ign_sci_list_dict,exegn_xci_mci_list_dict,\
        iegn_xci_mci_list_dict,ign_xci_mci_list_dict,ASE_variance
#%%
if __name__=="__main__":
    
    import matplotlib.pyplot as plt
    from TopologyAndTrafficManager import randomSimulation,setUpSpanPropertiesAccordingToTheLOI
    from os import mkdir
    
    modulationType = "cube4_16"
    _type_         = "predefined-network"
    
    PT_title="US_PhysicalTopology"
    PT_DB_dir=TM_DB_dir="Backbone_Data"
    TM_title="US_Total_TrafficMatrix"
    nuOffset=190e12
    slotBandwidth_GHz=25
    numOfGuardSlotsPerDemand=1
    
    span_approx_length_km=125
    
    nf=10**0.55
    ampgain="FC"
    numstep=1
    
    loi_id="dem4"
    
    lpdict,lspan_dict,nspan_dict=randomSimulation(modulationType=modulationType,_type_=_type_,
        PT_title=PT_title,PT_DB_dir=PT_DB_dir,TM_title=TM_title,TM_DB_dir=TM_DB_dir,nuOffset=nuOffset,
        slotBandwidth_GHz=slotBandwidth_GHz,numOfGuardSlotsPerDemand=numOfGuardSlotsPerDemand,
        span_approx_length_km=span_approx_length_km)
    
    alpha_dict,beta2_dict,gamma_dict,nf_dict,ampgain_dict,numstep_dict,acc_dispersion=\
        setUpSpanPropertiesAccordingToTheLOI(lpdict,loi_id,"SMF",nf,ampgain,numstep,lspan_dict,nspan_dict)
    
    alpha_list,beta2_list,gamma_list,lspan_list,ampgain_list,nf_list,\
        pair_list,power_at_bus_intersection_dict,first_common_node_dict,\
        Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,valid_pairs_per_model_types_dict=\
        preProcessing(lpdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
                      ampgain_dict,nf_dict,{demid: 1 for demid in lpdict})
    
    result_dir="asasasa"
    try:
        mkdir(result_dir)
    except:
        pass
    
    # egn_nli_terms=EGN_NLI_terms(lpdict,loi_id,alpha_list,beta2_list,gamma_list,lspan_list,
    #     first_common_node_dict,pair_list,Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,
    #     valid_pairs_per_model_types_dict,included_models=["Ex. EGN","iEGN","iGN"],nuMCp=1e1,
    #     progress_bar_position=0,is_progress_bar_enabled=1,result_dir=result_dir)
    
    power_set_dbm,exegn_snr_db_list_dict,iegn_snr_db_list_dict,ign_snr_db_list_dict,\
        exegn_sci_list_dict,iegn_sci_list_dict,ign_sci_list_dict,exegn_xci_mci_list_dict,\
        iegn_xci_mci_list_dict,ign_xci_mci_list_dict,ASE_variance=\
    SNR_per_num_side_channels(lpdict,{"0": lpdict},loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
            ampgain_dict,nf_dict,[0],1e1,0,1,result_dir)
    
    plt.figure()
    plt.plot(power_set_dbm,exegn_snr_db_list_dict["0"],"d-",label="Ex. EGN")
    plt.plot(power_set_dbm,iegn_snr_db_list_dict["0"],"s-",label="iEGN")
    plt.plot(power_set_dbm,ign_snr_db_list_dict["0"],"^-",label="iGN")
    plt.legend(fontsize=14)
    # pt=importPhysicalTopology("US_PhysicalTopology","Backbone_Data")
    # tm=importTrafficMatrix("US_Total_TrafficMatrix","Backbone_Data")
    
    # lpdict=Elastic_RWA(pt[0],pt[1],tm,slotBandwidth_GHz=25,
    #                    modulationType="cube4_16",nuOffset=190e12,numOfGuardSlotsPerDemand=1,
    #                    numOfRWAResults=1,maxNumOfCandidatePaths=1)[0]
    
    
# if __name__=='__main__':
    
#     # TODO The test part of this simulation (after the "if __name__=='__main__':" header) has fatal flaws. Needs to be resolved later.
    
#     # plt.close("all")
    
#     '''Tx and Rx parameters (optical-bus)'''
#     power_set_dbm=linspace(-10,10,101)
#     bw=50e9
# #    bw=32.7e9
#     sr=32e9
#     n_sym=1000
#     rrc_beta=0.02
#     mod_type='QPSK'
#     numlp=1
#     numlinks=5
#     num_lp_per_nu=4
#     max_num_out_of_bus_nodes=0
#     numch=7
#     loi_id='dem1'
#     rx_lp_list=['dem1']
    
#     '''Link parameters'''
#     fiber_type='SMF'
#     # fiber_type='NONLI'
#     # fiber_type='NONLIDISP'
#     # fiber_type='IDEAL'
#     lspan=100e3
#     nf=10**0.5
#     nspan=2
#     ampgain=None
    
#     '''Simulation parameters'''
#     launchpower=10**(0.1*0-3)
    
#     lpdict=randomSimulation(_type_='optical-bus',bw=bw,sr=sr,mod_type=mod_type,
#         numlp=numlp,numlinks=numlinks,num_lp_per_nu=num_lp_per_nu,numch=numch,nuOffset=0,
#         max_num_out_of_bus_nodes=max_num_out_of_bus_nodes)
    
#     launchpower_dict={lpid: launchpower for lpid in lpdict}
    
#     lpdictplot(lpdict,loi_id,bw,hgap=1,vgap=0.2,lvgap=0.03)
    
#     alpha_dict,beta2_dict,gamma_dict,lspan_dict,nspan_dict,ampgain_dict,nf_dict,_,acc_dispersion=\
#     randomSimulation(lpdict,loi_id,lspan,nspan,ampgain,nf,0,fiber_type)
    
#     alpha_list,beta2_list,gamma_list,lspan_list,ampgain_list,nf_list,\
#     pair_list,power_at_bus_intersection_dict,first_common_node_dict,Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT\
#     =preProcessing(lpdict,loi_id,alpha_dict,beta2_dict,
#                                         gamma_dict,lspan_dict,ampgain_dict,nf_dict,launchpower_dict)
    
#     NLI_terms1=EGN_NLI_terms(lpdict,loi_id,alpha_list,beta2_list,gamma_list,lspan_list,
#         first_common_node_dict,pair_list,Phi_dict,Psi_dict,ATTGAINDICT,BETA2DICT,model_type="general")
    
#     snr_db_list=[]
    
#     for power_dbm in power_set_dbm:
    
#         for lpid in lpdict:
#             power_at_bus_intersection_dict[lpid]=10**(0.1*power_dbm-3)
            
#         snr_db_list.append(
#             SNR(lpdict,NLI_terms1,ampgain_list,nf_list,loi_id,power_at_bus_intersection_dict,ATTGAINDICT)
#             )
        
#     # plt.figure()
#     # plt.plot(power_set_dbm,snr_db_list)