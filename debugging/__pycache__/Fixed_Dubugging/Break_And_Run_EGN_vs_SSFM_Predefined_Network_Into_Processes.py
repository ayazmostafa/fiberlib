# -*- coding: utf-8 -*-
"""
Created on Tue Dec 27 20:19:55 2022

@author: Mostafa
"""
# from subprocess import Popen,PIPE
# from asyncio import subprocess
from RWALib import Elastic_RWA,importPhysicalTopology,importTrafficMatrix
from json import loads,dump
from numpy.random import shuffle
from os import mkdir
import subprocess
#%%
if __name__=="__main__":
    
    ##########################################################################
    ##########################################################################
    #########################                       ##########################
    ######################### Adjustable parameters ##########################
    #########################                       ##########################
    ##########################################################################
    ##########################################################################
    
        # Topology and traffic information
    PT_TM_import_info=[
        ("Backbone_Data","Test1_PT","Test1_TM"),
        # ("Backbone_Data","US_PhysicalTopology","US_Total_TrafficMatrix"),
        # ("Backbone_Data","EU_PhysicalTopology","EU_Total_TrafficMatrix"),
        # ("Backbone_Data","GER_PhysicalTopology","GER_Total_TrafficMatrix"),
        ]
    
    random_number_of_loi_ids=10 # Set it to a high value to run the simulation for all the lightpaths.
    
    ##########################################################################
    ##########################################################################
    #####################                              #######################
    ##################### End of adjustable parameters #######################
    #####################                              #######################
    ##########################################################################
    ##########################################################################
    
    args_list=[]
    processes = []
    
    for PT_TM_DB_dir,PT_title,TM_title in PT_TM_import_info:
        
        nodes,linkLengths=importPhysicalTopology(PT_title,PT_TM_DB_dir)
        unnormalizedTrafficMatrix=importTrafficMatrix(TM_title,PT_TM_DB_dir)
        lpdict=Elastic_RWA(nodes,linkLengths,unnormalizedTrafficMatrix)[0]
        
        chosen_loi_ids=list(lpdict.copy().keys())
        for _ in range(len(lpdict)-random_number_of_loi_ids):
            shuffle(chosen_loi_ids)
            chosen_loi_ids.pop(0)
        
        # raise Exception("sadsadsad")
        # chosen_loi_ids=["dem47"]*10
        
        SNR_script = 'EGN_vs_SSFM_Predefined_Network.py'
        print("\fProcesses started.")

        for loi_id in chosen_loi_ids:
            args_list.append({
                # "power_range_db":power_range_db,
                # "rrc_beta":rrc_beta,
                # "nuMCp":nuMCp,
                # "numstep":numstep,
                # "n_sym":n_sym,
                # "upsratio_elevation":upsratio_elevation,
                # "sym_del_margin_ratio":sym_del_margin_ratio,
                # "Fs_type":Fs_type,
                "PT_TM_DB_dir":PT_TM_DB_dir,
                "PT_title":PT_title,
                "TM_title":TM_title,
                "loi_id":loi_id,
                # "modulationType":modulationType,
                # "nuOffset":nuOffset,
                # "slotBandwidth_GHz":slotBandwidth_GHz,
                # "numOfGuardSlotsPerDemand":numOfGuardSlotsPerDemand,
                # "span_approx_length_km":span_approx_length_km,
                # "ampgain":ampgain,
                # "fiber_type":fiber_type,
                # "nf":nf,
                # "results_parent_dir":results_parent_dir,
                # "progress_bar_position": chosen_loi_ids.index(loi_id),
                # "is_progress_bar_enabled": is_progress_bar_enabled,
                })
            processes.append(subprocess.Popen(["python",SNR_script,PT_TM_DB_dir,PT_title,TM_title,loi_id],stdout=subprocess.PIPE))

# else:

    exit_codes = [process.wait() for process in processes]
    
    # results=[process.stdout.read().decode().strip().replace("'","\"") for process in processes]
    
    results=[loads(process.stdout.read().decode().strip().replace("'","\"")) for process in processes]
    
    #%%
    for single_result,exit_code in zip(results,exit_codes):
        
        if not single_result["err"]=="":
            print(f"ERROR: {single_result['err']}")
            continue
        
        single_result_dir=single_result["result_dir"]
        
        if not exit_code==0:
            print("Flawed result detected. Marking flaw on saved data.")
            single_result_dir="[ERROR_ON_EXIT] "+single_result_dir
        
        try:
            mkdir(single_result["results_parent_dir"])
        except FileExistsError:
            pass
        # [print(f'python3 EGN_vs_SSFM_Predefined_Network.py "{args}"',end="\n\n") for args in args_list]
        # raise Exception("Hi----------------")

        mkdir(single_result_dir)
        
        for data_name in ['inputs', 'SNR_info', 'SCI_XCI_MCI_info']:
            dump(single_result[data_name],open(single_result_dir+"\\"+f"{data_name}.json","w+"))
        
        if not exit_code==0:
            dump(str(single_result["err"]),open(single_result_dir+"\\"+"error_log.json","w+"))