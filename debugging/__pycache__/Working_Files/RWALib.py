# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 01:01:02 2021

@author: Mostafa
"""


import networkx as nx
#import numpy as np
from numpy.random import choice,uniform,shuffle#,seed as ran_seed
# from BackBones import BackBones
from matplotlib.pyplot import savefig
from json import load
from numpy import arange,ceil,prod,floor,ones
# from tqdm import tqdm

import random
#ran_seed()
#%%
modulationBitsPerSymbol={"cube4_16":4}
#%%
def haveCommonLink(list1,list2):
    
    zip_list_2=list(zip(list2,list2[1:]))
    
    for i,j in zip(list1,list1[1:]):
        if (i,j) in zip_list_2 or (j,i) in zip_list_2:
            return True
    
    return False
#%%
def generateRandomGraph(numOfNodes,numOfLinks,linkLengthCandidates=[100],
                link_type='bidir',node_prefix='n'):
    
    setOfNodes={node_prefix+str(i) for i in range(1,1+numOfNodes)}
    dictOfLinks={}
    while True:
        s=choice(sorted(list(setOfNodes)))
        d=choice(sorted(list(setOfNodes-{s})))
        if not (s,d) in dictOfLinks:
            dictOfLinks[s,d]=choice(linkLengthCandidates)
            if link_type=='bidir':
                dictOfLinks[d,s]=dictOfLinks[s,d]
        if link_type=='bidir' and len(dictOfLinks)==2*numOfLinks:
            break
        if link_type=='unidir' and len(dictOfLinks)==numOfLinks:
            break
    
    return setOfNodes,dictOfLinks
#%%
def importPhysicalTopology(PT_title,PT_DB_dir):
    
    raw_data=load(open(PT_DB_dir+"/"+f"{PT_title}.json"))
    
    nodes=raw_data["nodes_coordinates"]
    
    links={tuple(link.split(",")):link_length_km for link,link_length_km in raw_data["link_lengths_km"].items()}
    
    links_copy=links.copy()
    
    for i,j in links_copy:
        links[j,i]=links[i,j]
    
    return set(nodes),links
#%%
def importTrafficMatrix(TM_title,TM_DB_dir):
    
    raw_data=load(open(TM_DB_dir+"/"+f"{TM_title}.json"))
    
    TrafficMatrix={}
    
    demand_index=1
    
    for src_dest in raw_data:
        
        src,dest=src_dest.split(",")
        
        if src==dest:
            continue
        
        TrafficMatrix[f"dem{demand_index}"]={
            "src": src,
            "dest": dest,
            "rate_gbps": raw_data[src_dest],
            "mod_type": None,
            "snr_req_db": None,
            }
        
        demand_index+=1
        
    return TrafficMatrix
#%%
def normalizeTrafficMatrix(unnormalizedTrafficMatrix,slotBandwidth_GHz,modulationType,numOfGuardSlotsPerDemand):
    normalizedTrafficMatrix={}
    for demid,demand in unnormalizedTrafficMatrix.items():
        normalizedTrafficMatrix[demid]={
            "src": demand["src"],
            "dest": demand["dest"],
            "num_of_active_slots": int(ceil(demand["rate_gbps"]/modulationBitsPerSymbol[modulationType]/slotBandwidth_GHz)),
            "total_num_of_slots": int(ceil(demand["rate_gbps"]/modulationBitsPerSymbol[modulationType]/slotBandwidth_GHz+numOfGuardSlotsPerDemand)),
            "mod_type": modulationType,
            "snr_req_db": None,
            }
        # modulationBitsPerSymbol[modulationType]
    return normalizedTrafficMatrix
#%%
def generateRandomTrafficMatrix(nodes,numOfDemands,rate_gbps=10,
        mod_type='QPSK',snr_req_db_interval=[12]):
    
    TrafficMatrix={}

    for index in range(numOfDemands):
        
        ID='dem'+str(index+1)
        
        s=choice(sorted(list(nodes)))
        
        d=choice(sorted(list(nodes-{s})))
        
        TrafficMatrix[ID]={
                'src': s,
                'dest': d,
                'rate_gbps': rate_gbps,
                'mod_type': mod_type,
                'snr_req_db': uniform(snr_req_db_interval[0],snr_req_db_interval[-1]),
                }
    
    return TrafficMatrix
#%%############################################################################
# Elastic wavelength assignment function
def occupySlotsOnLink(linkSlotOccupationPattern,minOccupiedSlotIndex,maxOccupiedSlotIndex):
    return linkSlotOccupationPattern*([1]*minOccupiedSlotIndex+\
            [0]*(maxOccupiedSlotIndex-minOccupiedSlotIndex)+\
            [1]*(len(linkSlotOccupationPattern)-maxOccupiedSlotIndex))
###############################################################################
def fetchAvailableSlotsAcrossLinks(occupiedSlotsPerLinks):
    return prod(occupiedSlotsPerLinks,axis=0)
###############################################################################
def occupyFirstSetOfAvailableSlots(slotOccupationPattern,numOfRequiredSlots):
    
    totalNumOfSlots=len(slotOccupationPattern)
    
    for i in range(totalNumOfSlots-numOfRequiredSlots+1):
        
        newOccupiedSlotsPattern=[1]*i+[0]*(numOfRequiredSlots)+[1]*(totalNumOfSlots-numOfRequiredSlots-i)
        
        if sum(newOccupiedSlotsPattern*slotOccupationPattern)==sum(slotOccupationPattern)-numOfRequiredSlots:
            return i,i+numOfRequiredSlots-1,newOccupiedSlotsPattern*slotOccupationPattern
    
    raise Exception(f"Slot allocation is not available due to lack of {numOfRequiredSlots} contiguous slots.")
#%%
def Elastic_RWA(nodes,linkLengths,unnormalizedTrafficMatrix,slotBandwidth_GHz=6.25,modulationType="cube4_16",nuOffset=190e12,numOfGuardSlotsPerDemand=1,numOfRWAResults=1,maxNumOfCandidatePaths=1):
    
    # Normalize the traffic matrix.
    normalizedTrafficMatrix=normalizeTrafficMatrix(
        unnormalizedTrafficMatrix,slotBandwidth_GHz,modulationType,numOfGuardSlotsPerDemand)
    
    lightpathdictlist=[]
    
    G = nx.DiGraph()
    G.add_nodes_from(nodes)
    for link,length in linkLengths.items():
        G.add_edge(link[0],link[1],weight=length)
    
    '''Routing'''
    demandPathListDict={}
    for demID in normalizedTrafficMatrix:
        s,d=normalizedTrafficMatrix[demID]['src'],normalizedTrafficMatrix[demID]['dest']
        if maxNumOfCandidatePaths==1:
            demandPathListDict[demID]=[nx.shortest_path(G,s,d,weight='weight')]
        else:
            demandPathListDict[demID]=list(nx.shortest_simple_paths(G,s,d,weight='weight'))
    
    '''Wavelength Assignment and Filling up the Variables'''
    # Full C-band contain 96, 50GHz channels. The total number of slots is then calculated by a division of C-band
    # capacity over slot bandwidth.
    
    slotBandwidth_Hz=slotBandwidth_GHz*1e9
    
    totalNumOfSlots=int(floor(96*50e9/slotBandwidth_Hz))
    
    for _ in range(numOfRWAResults):
        
        lightpathdict={}
        
        # Choose a path for each demand.
        demandPathDict={demID:random.choice(demandPathListDict[demID][:maxNumOfCandidatePaths]) for demID in demandPathListDict}
        
        # Set all the slots as unoccupied across all links.
        occupiedSlotsPerLinks={(i,j): ones(totalNumOfSlots) for demandPath in demandPathDict.values() for i,j in zip(demandPath,demandPath[1:])}
        
        # Start assigning slots to demands.
        for demID,demandPath in demandPathDict.items():
            
            # Fetch an available set of contiguous slots across all the links as much as the total number of slots.
            availableSlotsPatternAcrossLinks=fetchAvailableSlotsAcrossLinks([occupiedSlotsPerLinks[i,j] for i,j in zip(demandPath,demandPath[1:])])
            
            # Occupy first set of available slots.
            minOccupiedSlotIndex,maxOccupiedSlotIndex,_=\
                occupyFirstSetOfAvailableSlots(availableSlotsPatternAcrossLinks,normalizedTrafficMatrix[demID]["total_num_of_slots"])
            
            # Update the slots pattern of the path links using the minimum and maximum slot indices.
            for i,j in zip(demandPath,demandPath[1:]):
                occupiedSlotsPerLinks[i,j]=occupySlotsOnLink(
                    occupiedSlotsPerLinks[i,j],minOccupiedSlotIndex,maxOccupiedSlotIndex)
            
            # Occupy slots over links as many as the total number of slots.
            numOfDemandActiveSlots=normalizedTrafficMatrix[demID]["num_of_active_slots"]
            lightpathdict[demID]={
                "nodeList"             : demandPathDict[demID],
                "nu"                   : nuOffset+(minOccupiedSlotIndex+numOfDemandActiveSlots/2)*slotBandwidth_Hz,
                "chBandwidth"          : numOfDemandActiveSlots*slotBandwidth_Hz,
                "symbolRate"           : numOfDemandActiveSlots*slotBandwidth_Hz,
                "minOccupiedSlotIndex" : minOccupiedSlotIndex,
                "maxOccupiedSlotIndex" : maxOccupiedSlotIndex,
                "modulationType"       : modulationType,
                }
            # occupyFirstSetOfAvailableSlots(slotOccupationPattern,numOfRequiredSlots)
        
        # candidateLambdaIndices=arange(MAX_LAMBDA_INDEX)
        # if not numOfRWAResults==1:
        #     shuffle(candidateLambdaIndices)
        # candidateLambdaIndices=list(candidateLambdaIndices)
        
        # for i in range(len(demIDList)):
            
        #     demID=demIDList[i]
            
        #     availableLambdaIndices=candidateLambdaIndices.copy()
            
        #     for j in range(i):
        #         if haveCommonLink(demandPathDict[demID],demandPathDict[demIDList[j]]) and \
        #             assignedLambdaIndicesToDemands[demIDList[j]] in availableLambdaIndices:
        #             availableLambdaIndices.remove(assignedLambdaIndicesToDemands[demIDList[j]])
                    
        #     assignedLambdaIndicesToDemands[demID]=availableLambdaIndices[0]
        
        #     """Write into the variable."""
        #     lightpathdict[demID]={
        #             'nodeList'       : demandPathDict[demID],
        #             'nu'             : assignedLambdaIndicesToDemands[demID]*chBandwidth+nuOffset,
        #             "chBandwidth"    : chBandwidth,
        #             "symbolRate"     : symbolRate,
        #             "modulationType" : modulationType,
        #             }
        
        if not lightpathdict in lightpathdictlist:
            lightpathdictlist.append(lightpathdict)
        
    return lightpathdictlist
#%%
def RWA(nodes,linkLengths,trafficMatrix,chBandwidth=50e9,symbolRate=32e9,modulationType="QPSK",nuOffset=190e12,numOfResults=100,maxNumOfCandidatePaths=1000):
    
    MAX_LAMBDA_INDEX=100
    
    lightpathdictlist=[]
    
    G = nx.DiGraph()
    G.add_nodes_from(nodes)
    for link,length in linkLengths.items():
        G.add_edge(link[0],link[1],weight=length)
    
    '''Routing'''
    demandPathListDict={}
    for ID in trafficMatrix:
        print(f"Routing of demand {ID} of {len(trafficMatrix)} started.")
        s,d=trafficMatrix[ID]['src'],trafficMatrix[ID]['dest']
        if maxNumOfCandidatePaths==1:
            demandPathListDict[ID]=[nx.shortest_path(G,s,d,weight='weight')]
        else:
            demandPathListDict[ID]=list(nx.shortest_simple_paths(G,s,d,weight='weight'))
        print(f"Demand {ID} of {len(trafficMatrix)} routed.")
    
    '''Wavelength Assignment and Filling up the Variables'''
    
    demIDList=list(demandPathListDict.keys())
    
    for _ in range(numOfResults):
        
        assignedLambdaIndicesToDemands={}
        
        lightpathdict={}
        
        demandPathDict={demID:random.choice(demandPathListDict[demID][:maxNumOfCandidatePaths]) for demID in demandPathListDict}
        
        candidateLambdaIndices=arange(MAX_LAMBDA_INDEX)
        if not numOfResults==1:
            shuffle(candidateLambdaIndices)
        candidateLambdaIndices=list(candidateLambdaIndices)
        
        for i in range(len(demIDList)):
            
            demID=demIDList[i]
            
            availableLambdaIndices=candidateLambdaIndices.copy()
            
            for j in range(i):
                if haveCommonLink(demandPathDict[demID],demandPathDict[demIDList[j]]) and \
                    assignedLambdaIndicesToDemands[demIDList[j]] in availableLambdaIndices:
                    availableLambdaIndices.remove(assignedLambdaIndicesToDemands[demIDList[j]])
                    
            assignedLambdaIndicesToDemands[demID]=availableLambdaIndices[0]
        
            """Write into the variable."""
            lightpathdict[demID]={
                    'nodeList'       : demandPathDict[demID],
                    'nu'             : assignedLambdaIndicesToDemands[demID]*chBandwidth+nuOffset,
                    "chBandwidth"    : chBandwidth,
                    "symbolRate"     : symbolRate,
                    "modulationType" : modulationType,
                    }
        
        if not lightpathdict in lightpathdictlist:
            lightpathdictlist.append(lightpathdict)
        
    return lightpathdictlist
#%%
def drawNetwork(nodes,linkLengths,save_path_name=None):
    G=nx.Graph()
    G.add_nodes_from(nodes)
    G.add_edges_from(linkLengths)
    pos=nx.spring_layout(G)
    nx.draw(G,pos,with_labels = True,node_color='#bbbbbb',font_size=13,node_size=500)
    nx.draw_networkx_edge_labels(G,pos,edge_labels=linkLengths,font_color='red',rotate=False)
    if not save_path_name==None:
        savefig(save_path_name,dpi=200)
#%%
if __name__=='__main__':

    print("\f")
#    set_of_nodes={1,2,3,4,5}
#    dict_of_edges={
#            (1, 2): 100,
#            (2, 3): 100,
#            (3,4): 100,
#            (1,5): 100,
#            (5,4): 100,
#            }
    
#    set_of_nodes,dict_of_edges=GenerateRandomGraph(7,10)
    set_of_nodes,dict_of_edges=importPhysicalTopology('US_PhysicalTopology',"Backbone_Data")
    
    # drawNetwork(set_of_nodes,dict_of_edges,'aaaaaaaaaaa')
    
    TM=generateRandomTrafficMatrix(set_of_nodes,10)
    
#    lightpathdict,dpd,dl,al=RWA(set_of_nodes,dict_of_edges,TM)
    lightpathdict=RWA(set_of_nodes, dict_of_edges, TM,chBandwidth=50e9,symbolRate=32e9,modulationType="QPSK",nuOffset=190e12,numOfResults=100,maxNumOfCandidatePaths=1)[0]
#    nx.draw(G)
    file=open('test_lp.csv','w+')
    file.writelines('Lightpath ID,Node List,Wavelength\n')
    for lpid,lp in lightpathdict.items():
        file.writelines('{},{},{}\n'.format(
                lpid,'---'.join([str(inode) for inode in lp['nodeList']]),lp['nu']
                ))
    file.close()
    
    
    lightpathdictlist=RWA(set_of_nodes, dict_of_edges, TM,chBandwidth=50e9,symbolRate=32e9,modulationType="QPSK",nuOffset=190e12,numOfResults=100,maxNumOfCandidatePaths=2)
    
    # for lpdict in lightpathdictlist:
    #     print("#######################################")
    #     for demID in lpdict:
    #         print(demID)
    #         print(f"\t{lpdict[demID]['nodeList']}")
    #         print(f"\t{lpdict[demID]['nu']}")
    
    
    
    
    
    
    
    
    
    
    
    
    x=importTrafficMatrix('US_Total_TrafficMatrix',"Backbone_Data")
    # y=normalizeTrafficMatrix(set_of_nodes,dict_of_edges,slotBandwidth_GHz=12.5,modulationType="QPSK",numOfGuardSlotsPerDemand=1)
    z=Elastic_RWA(set_of_nodes,dict_of_edges,x,slotBandwidth_GHz=12.5,modulationType="cube4_16",nuOffset=190e12,numOfGuardSlotsPerDemand=1,numOfRWAResults=1,maxNumOfCandidatePaths=1)