# -*- coding: utf-8 -*-
"""
Created on Fri Jan  6 22:09:27 2023

@author: Mostafa
"""

from RWALib import importPhysicalTopology,importTrafficMatrix,Elastic_RWA
PT_TM_import_info=[
    # ("Backbone_Data","Test1_PT","Test1_TM"),
    # ("Backbone_Data","US_PhysicalTopology","US_Total_TrafficMatrix"),
    # ("Backbone_Data","EU_PhysicalTopology","EU_Total_TrafficMatrix"),
    ("Backbone_Data","GER_PhysicalTopology","GER_Total_TrafficMatrix"),
    ]
for PT_TM_DB_dir,PT_title,TM_title in PT_TM_import_info:
    nodes,linkLengths=importPhysicalTopology(PT_title,PT_TM_DB_dir)
    unnormalizedTrafficMatrix=importTrafficMatrix(TM_title,PT_TM_DB_dir)
    lpdict=Elastic_RWA(nodes,linkLengths,unnormalizedTrafficMatrix)[0]

lpdict_filtered={lpid:lp for lpid,lp in lpdict.items() if len(lp["nodeList"])>=5+1}


# US chosen LOI IDs (Lightpath link number >= 4)
chosen_loi_ids=['dem3', 'dem5', 'dem10', 'dem18', 'dem20', 'dem22', 'dem29', 'dem34', 'dem35',
   'dem40', 'dem42','dem46', 'dem52', 'dem66', 'dem67', 'dem85', 'dem89', 'dem93', 'dem95', 'dem98',
   'dem104', 'dem107','dem119', 'dem120', 'dem131', 'dem142', 'dem150', 'dem167', 'dem173', 'dem177']

# EU chosen LOI IDs (Lightpath link number >= 8)
chosen_loi_ids=['dem57', 'dem72', 'dem76', 'dem79', 'dem84', 'dem86', 'dem94', 'dem96', 'dem139', 'dem157',
    'dem160', 'dem204', 'dem355', 'dem373', 'dem376', 'dem400', 'dem409', 'dem413', 'dem423', 'dem427',
    'dem430', 'dem454', 'dem489', 'dem502', 'dem597', 'dem600', 'dem608', 'dem609', 'dem610', 'dem611',
    'dem678', 'dem681', 'dem689', 'dem691']

# GER chosen LOI IDs (Lightpath link number >= 8)
chosen_loi_ids=['dem29', 'dem30', 'dem44', 'dem45', 'dem59', 'dem60', 'dem68', 'dem72', 'dem74', 'dem75',
    'dem105','dem106', 'dem121', 'dem126', 'dem151', 'dem186', 'dem214', 'dem215', 'dem216', 'dem217',
    'dem219','dem229', 'dem230', 'dem231', 'dem232', 'dem234', 'dem235', 'dem237']

