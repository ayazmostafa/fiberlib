# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 16:17:50 2022

@author: Mostafa
"""

from RWALib import generateRandomGraph,importPhysicalTopology,importTrafficMatrix,\
generateRandomTrafficMatrix,RWA,Elastic_RWA#,DrawNetwork
from numpy.random import choice,permutation,randint,rand
from numpy import cumsum,exp,ceil
# from FiberTypeDB import FIBER_SPEC
from json import load
from time import ctime
from os.path import split as ospathsplit
#%%
FIBER_SPEC={
        'SMF': {
                'alpha': 0.22/4343,
                'beta2': -21.3e-27,
                'gamma': 1.3e-3
                },
        "NZ-DSF": {
                "alpha": 0.22/4343,
                "beta2": -4.85e-27,
                "gamma": 1.5e-3
                },
        'NONLI': {
                'alpha': 0.22/4343,
                'beta2': -21.3e-27,
                'gamma': 0
                },
        'NODISP': {
                'alpha': 0.22/4343,
                'beta2': 0,
                'gamma': 1.3e-3
                },
        'NOATT': {
                'alpha': 0,
                'beta2': -21.3e-27,
                'gamma': 1.3e-3
                },
        'NONLIDISP': {
                'alpha': 0.22/4343,
                'beta2': 0,
                'gamma': 0
                },
        'IDEAL': {
                'alpha': 0,
                'beta2': 0,
                'gamma': 0
                },
        }
#%%
def createRandomHash(hashlength=20):
    # hash_alphabet="qwertyuioplaksjdhfgmznxbcvQWERTYUIOPLAKSJDHFGMZNXBCV0123456789"
    hash_alphabet="qwertyuioplaksjdhfgmznxbcv0123456789"
    return "".join(choice(",".join(hash_alphabet).split(","),hashlength))
#%%
def randomSimulation(**kwargs):
    
    lpdict       = {}
    lspan_dict   = {}
    nspan_dict   = {}
    # all_links    = set()
    
    modulationType = kwargs['modulationType']
    
    _type_         = kwargs["_type_"]
    
    if _type_=='single-link':
        
        chBandwidth    = kwargs['chBandwidth']
        symbolRate     = kwargs['symbolRate']
        numch          = kwargs['numch']
        
        numlinks=1
        
        if 'loi_nu_index' in kwargs:
            loi_nu_index = kwargs['loi_nu_index']
        else:
            loi_nu_index = int(0.5+0.5*numch)
        
        temp_neighbor_lps=[]
        
        for lpid in set(range(1,1+numch))-{loi_nu_index}:
            temp_neighbor_lps.append({
                    'nodeList'        : ['node1','node2'],
                    'nu'              : (lpid)*chBandwidth,
                    'chBandwidth'     : chBandwidth,
                    'symbolRate'      : symbolRate,
                    'modulationType'  : modulationType,
                    })
        
        lpdict["dem1"]={
                'nodeList'        : ['node1','node2'],
                'nu'              : loi_nu_index*chBandwidth,
                'chBandwidth'     : chBandwidth,
                'symbolRate'      : symbolRate,
                'modulationType'  : modulationType,
                }
        for lpid,lp in enumerate(temp_neighbor_lps):
            lpdict[f"dem{lpid+2}"]=lp
        
        """Determining link lengths"""
        nspan=kwargs["nspan"]
        lspan_candidate_list = kwargs["lspan_candidate_list"]
        for i in range(numlinks):
            lspan_dict[f"node{i+1}",f"node{i+2}"]=[]
            for _ in range(nspan):
                lspan_dict[f"node{i+1}",f"node{i+2}"].append(lspan_candidate_list[0][sum(rand()<cumsum(lspan_candidate_list[1][::-1]))-1])
    
    elif _type_=='optical-bus':
        
        chBandwidth   = kwargs['chBandwidth']
        symbolRate    = kwargs['symbolRate']
        numlp         = kwargs['numlp']
        numch         = kwargs['numch']
        numlinks      = kwargs['numlinks']
        nuOffset      = kwargs['nuOffset']
        num_lp_per_nu = kwargs['num_lp_per_nu']
        
        if 'loi_nu_index' in kwargs:
            loi_nu_index = kwargs['loi_nu_index']
        else:
            loi_nu_index = int(0.5+0.5*numch)
        
        if num_lp_per_nu > numlinks:
            raise Exception(f'num_lp_per_nu ({num_lp_per_nu}) should NOT be greater than numlinks ({numlinks}).')
        
        if numlp>num_lp_per_nu*(numch-1)+1:
            raise Exception(f"Too many lightpaths for num_lp_per_nu={num_lp_per_nu} and numch={numch}.")
        
        lpdict={"dem1": {
                        "nodeList"        : [f"node{k}" for k in range(1,1+1+numlinks)],
                        "nu"              : loi_nu_index*chBandwidth+nuOffset,
                        'chBandwidth'     : chBandwidth,
                        'symbolRate'      : symbolRate,
                        'modulationType'  : modulationType,
                        }}
        
        list_of_all_but_first_and_last_bus_node_indices=list(range(2,1+numlinks))
        
        temp_lps=[]
        
        """Establishing lightpaths per channel"""
        for l in set(range(1,1+numch))-{loi_nu_index}:
            """Establishing set of consecutive nodes per channel"""
            while True:
                temp_nodes_per_nu=choice(list_of_all_but_first_and_last_bus_node_indices,num_lp_per_nu-1)
                if len(set(temp_nodes_per_nu))==num_lp_per_nu-1:
                    temp_nodes_per_nu=[1]+sorted(list(temp_nodes_per_nu))+[1+numlinks]
                    break
            """Converting each node section to a single lightpath"""
            for i,j in zip(temp_nodes_per_nu,temp_nodes_per_nu[1:]):
                temp_lps.append({
                        "nodeList"        : [f"node{k}" for k in range(i,j+1)],
                        "nu"              : l*chBandwidth+nuOffset,
                        'chBandwidth'     : chBandwidth,
                        'symbolRate'      : symbolRate,
                        'modulationType'  : modulationType,
                        })
              
        """Permuting temp lightpaths for future removal"""
        temp_lps=list(permutation(temp_lps))
        
        """Removing extra lightpaths"""
        for ind in range(2,1+numlp):
            lpdict[f"dem{ind}"]=temp_lps[-1]
            temp_lps.pop(-1)
        
        """Determining link lengths"""
        nspan=kwargs["nspan"]
        lspan_candidate_list = kwargs["lspan_candidate_list"]
        for i in range(numlinks):
            nspan_dict[f"node{i+1}",f"node{i+2}"]=nspan
            lspan_dict[f"node{i+1}",f"node{i+2}"]=[]
            for _ in range(nspan):
                lspan_dict[f"node{i+1}",f"node{i+2}"].append(lspan_candidate_list[0][sum(rand()<cumsum(lspan_candidate_list[1][::-1]))-1])
                
            
    elif _type_=="predefined-network":
        
        nodes,link_length_km_dict = importPhysicalTopology(kwargs["PT_title"],kwargs["PT_DB_dir"])
        trafficMatrix             = importTrafficMatrix(kwargs["TM_title"],kwargs["TM_DB_dir"])
        nuOffset                  = kwargs["nuOffset"]
        # is_modified_link_length   = kwargs["is_modified_link_length"]
        slotBandwidth_GHz         = kwargs["slotBandwidth_GHz"]
        numOfGuardSlotsPerDemand  = kwargs["numOfGuardSlotsPerDemand"]
        
        # if is_modified_link_length:
        #     link_length_km_dict={(i,j): max([round(link_length_km_dict[i,j],-2),100]) for i,j in link_length_km_dict}
        
        lpdict=Elastic_RWA(nodes,link_length_km_dict,trafficMatrix,slotBandwidth_GHz=slotBandwidth_GHz,
                           modulationType=modulationType,nuOffset=nuOffset,
                           numOfGuardSlotsPerDemand=numOfGuardSlotsPerDemand,
                           numOfRWAResults=1,maxNumOfCandidatePaths=1)[0]
        
        """Determining link lengths"""
        span_approx_length_km=kwargs["span_approx_length_km"]
        for i,j in link_length_km_dict:
            nspan=int(ceil(link_length_km_dict[i,j]/span_approx_length_km))
            # lspan=link_length_km_dict[i,j]/nspan
            lspan_dict[i,j]=[span_approx_length_km*1e3]*(nspan-1)+[(link_length_km_dict[i,j]-span_approx_length_km*(nspan-1))*1e3]
            nspan_dict[i,j]=nspan
        
        # """Determining link lengths"""
        # span_approx_length_km=kwargs["span_approx_length_km"]
        # for i,j in link_length_km_dict:
        #     nspan=max(round(link_length_km_dict[i,j]/span_approx_length_km),1)
        #     lspan=link_length_km_dict[i,j]/nspan
        #     lspan_dict[i,j]=[lspan]*nspan
    
    elif _type_=="predefined-network-random-tm":
        chBandwidth    = kwargs['chBandwidth']
        symbolRate     = kwargs['symbolRate']
        num_lp         = kwargs['numlp']
        PT_label=kwargs['PT_label']
        # chbandwidth             = kwargs["chbandwidth"]
        nuOffset                = kwargs["nuOffset"]
        # numOfResults            = kwargs["numOfResults"]
        # maxNumOfCandidatePaths  = kwargs["maxNumOfCandidatePaths"]
        is_modified_link_length = kwargs["is_modified_link_length"]
        nodes,link_length_km_dict=importPhysicalTopology(PT_label)
        if is_modified_link_length:
            for i,j in link_length_km_dict:
                link_length_km_dict[i,j]=max([round(link_length_km_dict[i,j],-2),100])
        trafficMatrix=generateRandomTrafficMatrix(nodes,num_lp)
        lpdict=RWA(nodes,link_length_km_dict,trafficMatrix,chBandwidth,symbolRate,modulationType,nuOffset,1,1)[0]
        
        """Determining link lengths"""
        span_approx_length_km=kwargs["span_approx_length_km"]
        for i,j in link_length_km_dict:
            nspan=max(round(link_length_km_dict[i,j]/span_approx_length_km),1)
            lspan_km=link_length_km_dict[i,j]/nspan
            lspan_dict[i,j]=[lspan_km*1e3]*nspan
    
    elif _type_=="random-network":
        chBandwidth    = kwargs['chBandwidth']
        symbolRate     = kwargs['symbolRate']
        num_lp         = kwargs['numlp']
        num_of_nodes=kwargs['num_of_nodes']
        num_of_links=kwargs['num_of_links']
        # chbandwidth             = kwargs["chbandwidth"]
        nuOffset                = kwargs["nuOffset"]
        # numOfResults            = kwargs["numOfResults"]
        # maxNumOfCandidatePaths  = kwargs["maxNumOfCandidatePaths"]
        link_length_candidate=kwargs["link_length_candidate_km"]
        nodes,link_length_km_dict=generateRandomGraph(num_of_nodes,num_of_links,
                    link_length_candidate,link_type='bidir')
        trafficMatrix=generateRandomTrafficMatrix(nodes,num_lp)
        lpdict=RWA(nodes,link_length_km_dict,trafficMatrix,chBandwidth,symbolRate,modulationType,nuOffset,1,1)[0]
        
        """Determining link lengths"""
        span_approx_length_km=kwargs["span_approx_length_km"]
        for i in range(numlinks):
            nspan=max(round(link_length_km_dict[f"node{i+1}",f"node{i+2}"]/span_approx_length_km),1)
            lspan=link_length_km_dict[i,j]/nspan
            lspan_dict[f"node{i+1}",f"node{i+2}"]=[lspan]*nspan
        
    else:
        raise Exception('Invalid lpdict type.')
    
    return lpdict,lspan_dict,nspan_dict
#%%
def setUpSpanPropertiesAccordingToTheLOI(lpdict,loi_id,fiber_type,nf,ampgain,numstep,lspan_dict,nspan_dict):
    
    alpha_dict   = {}
    beta2_dict   = {}
    gamma_dict   = {}
    nf_dict      = {}
    ampgain_dict = {}
    numstep_dict = {}
    all_links    = set()
    
    """Topology details"""
    
    loi_link_list=list(zip(lpdict[loi_id]['nodeList'],lpdict[loi_id]['nodeList'][1:]))
    
    for lpid,lp in lpdict.items():
        for i,j in zip(lp['nodeList'],lp['nodeList'][1:]):
            all_links.add((i,j))
    
    for i,j in all_links:
        
        if (i,j) in loi_link_list:
            link_fiber_type=fiber_type
            span_nf=nf
        else:
            link_fiber_type='NONLI'
            span_nf=0
        
        alpha_dict   [i,j] = []
        beta2_dict   [i,j] = []
        gamma_dict   [i,j] = []
        nf_dict      [i,j] = []
        ampgain_dict [i,j] = []
        numstep_dict [i,j] = []
        
        for n in range(nspan_dict[i,j]):
            
            '''alpha'''
            alpha=FIBER_SPEC[link_fiber_type]['alpha']
            alpha_dict[i,j].append(alpha)
            
            '''beta2'''
            beta2_dict[i,j].append(FIBER_SPEC[link_fiber_type]['beta2'])
            
            '''gamma'''
            gamma_dict[i,j].append(FIBER_SPEC[link_fiber_type]['gamma'])
            
            '''NF'''
            nf_dict[i,j].append(span_nf)
            
            '''ampgain'''
            if ampgain=="FC":
                ampgain_dict[i,j].append(exp(alpha*lspan_dict[i,j][n]))
            else:
                ampgain_dict[i,j].append(ampgain)
            
            '''numstep'''
            if gamma_dict[i,j][n]==0 or beta2_dict[i,j][n]==0:
                numstep_dict[i,j].append(1)
            else:
                numstep_dict[i,j].append(numstep)
        
    acc_dispersion=sum([
            beta2_dict[i,j][n]*lspan_dict[i,j][n] for i,j in loi_link_list\
            for n in range(nspan_dict[i,j])
            ])
    
    return alpha_dict,beta2_dict,gamma_dict,nf_dict,ampgain_dict,numstep_dict,acc_dispersion
#%%
def createRandomSimulationForOpticalBus(parent_folder):
    
    """
    This function first tries to identify any previously generated result in the folder.
    If such a result was not found, this function produces a new one from the params.json file.
    """
    
    # Check if any produced results exist in the parent_folder.
    try:
        
        lpdict        = load(open(parent_folder+"/lpdict.json","r"))
        topology_info = load(open(parent_folder+"/topology_info.json","r"))
        inputs        = load(open(parent_folder+"/inputs.json","r"))
        
        power_range_db           = inputs["power_range_db"]
        chBandwidth              = inputs["chBandwidth"]
        symbolRate               = inputs["symbolRate"]
        modulationType           = inputs["modulationType"]
        numlinks                 = inputs["numlinks"]
        num_lp_per_nu            = inputs["num_lp_per_nu"]
        numch                    = inputs["numch"]
        numlp                    = inputs["numlp"]
        lspan_candidate_list     = inputs["lspan_candidate_list"]
        fiber_type               = inputs["fiber_type"]
        nf                       = inputs["nf_LIN"]
        rrc_beta                 = inputs["rrc_beta"]
        loi_id                   = inputs["loi_id"]
        nspan                    = inputs["nspan"]
        nuOffset                 = inputs["nuOffset"]
        max_num_out_of_bus_nodes = inputs["max_num_out_of_bus_nodes"]
        ampgain                  = inputs["ampgain"]
        nuMCp                    = inputs["nuMCp"]
        n_sym                    = inputs["n_sym"]
        sym_del_margin_ratio     = inputs["sym_del_margin_ratio"]
        numstep                  = inputs["numstep"]
        upsratio_elevation       = inputs["upsratio_elevation"]
        Fs_type                  = inputs["Fs_type"]
        
        alpha_dict     = {}
        beta2_dict     = {}
        gamma_dict     = {}
        lspan_dict     = {}
        nspan_dict     = {}
        ampgain_dict   = {}
        nf_dict        = {}
        numstep_dict   = {}
        acc_dispersion = topology_info["acc_dispersion"]
        
        for key in topology_info["alpha_dict"]:
            if key=="acc_dispersion":
                continue
            i,j=key.split(",")
            alpha_dict   [i,j] = topology_info["alpha_dict"]   [key]
            beta2_dict   [i,j] = topology_info["beta2_dict"]   [key]
            gamma_dict   [i,j] = topology_info["gamma_dict"]   [key]
            lspan_dict   [i,j] = topology_info["lspan_dict"]   [key]
            nspan_dict   [i,j] = topology_info["nspan_dict"]   [key]
            ampgain_dict [i,j] = topology_info["ampgain_dict"] [key]
            nf_dict      [i,j] = topology_info["nf_dict"]      [key]
            numstep_dict [i,j] = topology_info["numstep_dict"] [key]
            
        results_dir=ospathsplit(parent_folder)[0]+"/"+ctime().replace(':','.')+" #"+createRandomHash()+" @reproduced_from@ #"+parent_folder.split("#")[-1]#.split("/")[-1]
        
        print(f"Generated results found in {parent_folder}. Reproducing the results...")
    
    except FileNotFoundError as err:
        
        print(err)
        print("Retrieving simulation information from params.json ...")
        
        chooseRandomly=lambda list_of_lists: list_of_lists[randint(0,len(list_of_lists))]
        
        params_raw=load(open(parent_folder+"/params.json","r"))
        
        power_range_db           = chooseRandomly(params_raw["power_range_db_choices"])
        chBandwidth              = chooseRandomly(params_raw["chBandwidth_choices"])
        symbolRate               = chooseRandomly(params_raw["symbolRate_choices"])
        modulationType           = chooseRandomly(params_raw["modulationType_choices"])
        numlinks                 = chooseRandomly(params_raw["numlinks_choices"])
        num_lp_per_nu            = chooseRandomly(params_raw["num_lp_per_nu_choices"])
        numch                    = chooseRandomly(params_raw["numch_choices"])
        numlp                    = chooseRandomly(params_raw["numlp_choices"])
        lspan_candidate_list     = chooseRandomly(params_raw["lspan_candidate_list_choices"])
        fiber_type               = chooseRandomly(params_raw["fiber_type_choices"])
        nf                       = chooseRandomly(params_raw["nf_choices"])
        rrc_beta                 = chooseRandomly(params_raw["rrc_beta_choices"])
        loi_id                   = chooseRandomly(params_raw["loi_id_choices"])
        nspan                    = chooseRandomly(params_raw["nspan_choices"])
        nuOffset                 = chooseRandomly(params_raw["nuOffset_choices"])
        max_num_out_of_bus_nodes = chooseRandomly(params_raw["max_num_out_of_bus_nodes_choices"])
        ampgain                  = chooseRandomly(params_raw["ampgain_choices"])
        nuMCp                    = chooseRandomly(params_raw["nuMCp_choices"])
        n_sym                    = chooseRandomly(params_raw["n_sym_choices"])
        sym_del_margin_ratio     = chooseRandomly(params_raw["sym_del_margin_ratio_choices"])
        numstep                  = chooseRandomly(params_raw["numstep_choices"])
        upsratio_elevation       = chooseRandomly(params_raw["upsratio_elevation_choices"])
        Fs_type                  = chooseRandomly(params_raw["Fs_type_choices"])
        
        lpdict,alpha_dict,beta2_dict,gamma_dict,lspan_dict,nspan_dict,ampgain_dict,nf_dict,\
        numstep_dict,acc_dispersion=randomSimulation(
            chBandwidth=chBandwidth,symbolRate=symbolRate,modulationType=modulationType,numlp=numlp,
            numlinks=numlinks,num_lp_per_nu=num_lp_per_nu,numch=numch,lspan_candidate_list=lspan_candidate_list,
            nspan=nspan,fiber_type=fiber_type,nuOffset=nuOffset,max_num_out_of_bus_nodes=max_num_out_of_bus_nodes,
            _type_="optical-bus",loi_id=loi_id,ampgain=ampgain,nf=nf,numstep=numstep)
        
        results_dir=parent_folder+"/"+ctime().replace(':','.')+" #"+createRandomHash()
        
    return power_range_db,chBandwidth,symbolRate,modulationType,numlinks,num_lp_per_nu,numch,\
    numlp,lspan_candidate_list,fiber_type,nf,rrc_beta,loi_id,nspan,nuOffset,max_num_out_of_bus_nodes,\
    ampgain,nuMCp,n_sym,sym_del_margin_ratio,numstep,upsratio_elevation,Fs_type,lpdict,alpha_dict,\
    beta2_dict,gamma_dict,lspan_dict,nspan_dict,ampgain_dict,nf_dict,numstep_dict,acc_dispersion,results_dir
#%%

# def Topology(lpdict,loi_id,lspan_candidate_list,nspan,ampgain,nf,numstep,fiber_type):
    
#     alpha_dict={}
#     beta2_dict={}
#     gamma_dict={}
#     lspan_dict={}
#     nf_dict={}
#     ampgain_dict={}
#     numstep_dict={}
#     nspan_dict={}
#     all_links=set()
#     loi_link_list=list(zip(lpdict[loi_id]['nodelist'],lpdict[loi_id]['nodelist'][1:]))
    
#     for lpid,lp in lpdict.items():
#         for i,j in zip(lp['nodelist'],lp['nodelist'][1:]):
#             all_links.add((i,j))
    
#     for i,j in all_links:
        
#         if (i,j) in loi_link_list:
#             link_fiber_type=fiber_type
#             span_nf=nf
#         else:
#             link_fiber_type='NONLI'
#             span_nf=0
        
#         alpha_dict[i,j]=[]
#         beta2_dict[i,j]=[]
#         gamma_dict[i,j]=[]
#         lspan_dict[i,j]=[]
#         nf_dict[i,j]=[]
#         ampgain_dict[i,j]=[]
#         numstep_dict[i,j]=[]
        
#         for n in range(nspan):
            
#             lspan=lspan_candidate_list[0][
#                     sum(rand()<cumsum(lspan_candidate_list[1][::-1]))-1]
            
#             '''alpha'''
#             alpha=FIBER_SPEC[link_fiber_type]['alpha']
#             alpha_dict[i,j].append(alpha)
            
#             '''beta2'''
#             beta2_dict[i,j].append(FIBER_SPEC[link_fiber_type]['beta2'])
            
#             '''gamma'''
#             gamma_dict[i,j].append(FIBER_SPEC[link_fiber_type]['gamma'])
            
#             '''lspan'''
#             lspan_dict[i,j].append(lspan)
            
#             '''nf'''
#             nf_dict[i,j].append(span_nf)
            
#             '''ampgain'''
#             if ampgain==None:
#                 ampgain_dict[i,j].append(exp(alpha*lspan))
#             else:
#                 ampgain_dict[i,j].append(ampgain)
            
#             '''numstep'''
#             if gamma_dict[i,j][n]==0 or beta2_dict[i,j][n]==0:
#                 numstep_dict[i,j].append(1)
#             else:
#                 numstep_dict[i,j].append(numstep)
        
#         '''nspan'''
#         nspan_dict[i,j]=nspan
    
#     acc_dispersion=sum([
#             beta2_dict[i,j][n]*lspan_dict[i,j][n]\
#             for i,j in loi_link_list\
#             for n in range(nspan_dict[i,j])
#             ])
    
#     return alpha_dict,beta2_dict,gamma_dict,lspan_dict,nspan_dict,\
#         ampgain_dict,nf_dict,numstep_dict,acc_dispersion