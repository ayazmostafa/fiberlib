# -*- coding: utf-8 -*-
"""
Created on Sat Jan  7 13:22:43 2023

@author: Mostafa
"""

# -*- coding: utf-8 -*-
"""
Created on Tue May 31 12:43:23 2022

@author: smosaya
"""

from SSFM import SNR as SNR_SSFM#,lpdictplot,elastic_lpdictplot
from NLI_modeling import SNR_per_num_side_channels as SNR_EGN_per_num_side_channels
# from NLI_modeling_multiprocessing import SNR_per_num_side_channels as SNR_EGN_per_num_side_channels
from time import ctime
from os import mkdir
from json import dump
# from json import loads
# from TopologyAndTrafficManager import randomSimulation,setUpSpanPropertiesAccordingToTheLOI
# from sys import argv
from numpy.random import choice
from numpy import exp
# import matplotlib.pyplot as plt
# from os import system
#%%
def createRandomHash(hashlength=20):
    # hash_alphabet="qwertyuioplaksjdhfgmznxbcvQWERTYUIOPLAKSJDHFGMZNXBCV0123456789"
    hash_alphabet="qwertyuioplaksjdhfgmznxbcv0123456789"
    return "".join(choice(",".join(hash_alphabet).split(","),hashlength))
#%%
if __name__=="__main__":
    
    # Transceiver parameters
    numch=1
    bandwidth=33e9
    symbolrate=33e9
    modulationType="cube4_16"
    nuOffset=190e12
    power_range_db=[0] # Range of power around the optimum power. Set it to [0] for optimum power of maximum SNR.
    rrc_beta=0.02 # Root-raised cosine roll-off factor.
    loi_id                   = "dem1"
    
    # Span parameters
    alpha=0.22/4343
    beta2=-21e-27
    gamma=1.3e-3
    nf=10**0.55
    lspan=100e3
    numspan=1
    ampgain=exp(alpha*lspan)
    
    # NLI-modeling parameters
    nuMCp=1e5 # Number of Monte-Carlo integration points. Keep it high to avoid NaN-valued SNRs.
    
    # SSFM parameters
    n_sym=1.5e5*0+5000 # Number of symbols at transmitter.
    upsratio_elevation=2 # Upsampling ratio elevation.
    sym_del_margin_ratio=4000/n_sym # Number of expendable marginal symbols.
    Fs_type="total" # Sampling frequency type.
    numstep=200
    
    
    
    lpdict={f"dem{i}": {
        "nodeList": ["node1","node2"],
        'nu': nuOffset+i*bandwidth,
        'chBandwidth': bandwidth,
        'symbolRate': symbolrate,
        'modulationType': modulationType,
        } for i in range(1,1+numch)}
    alpha_dict={("node1","node2"): [alpha]*numspan}
    beta2_dict={("node1","node2"): [beta2]*numspan}
    gamma_dict={("node1","node2"): [gamma]*numspan}
    nf_dict={("node1","node2"): [nf]*numspan}
    ampgain_dict={("node1","node2"): [ampgain]*numspan}
    numstep_dict={("node1","node2"): [numstep]*numspan}
    acc_dispersion=beta2*numspan*lspan
    lspan_dict={("node1","node2"): [lspan]*numspan}
    nspan_dict={("node1","node2"): numspan}
    
    results={}
    
    try:
        
        slotBandwidth_GHz=symbolrate
        is_progress_bar_enabled=1
        progress_bar_position=int(loi_id[3:])
        
        results_parent_dir="Results_Set_Single_Link_1"
        
        creation_date=ctime()
        
        process_id=createRandomHash()
        
        result_dir=results_parent_dir+"/"+process_id
        
        model_lpdictdict={"0":lpdict}
        
        try:
            mkdir(results_parent_dir)
        except FileExistsError:
            pass

        mkdir(result_dir)
        #%%#########################################################
        ############################################################
        #######################              #######################
        #######################     EGN      #######################
        #######################              #######################
        ############################################################
        ############################################################
        
        ############################################################
        ################# Limited neighbor channels ################
        ############################################################
        # print("Now starting model simulation...")
        power_set_dbm,___,egn_snr_db_list_dict,ign_snr_db_list_dict,\
            _,egn_sci_list_dict,ign_sci_list_dict,__,\
            egn_xci_mci_list_dict,ign_xci_mci_list_dict,ASE_variance=SNR_EGN_per_num_side_channels(lpdict,
            model_lpdictdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,ampgain_dict,nf_dict,
            power_range_db,nuMCp,progress_bar_position,is_progress_bar_enabled,result_dir)
        # print("Model simulation completed.")
        #%%#########################################################
        ############################################################
        #######################               ######################
        #######################     SSFM      ######################
        #######################               ######################
        ############################################################
        ############################################################
        
        ############################################################
        #####################   SCI + XCI + MCI   ##################
        ############################################################
        # print("Now starting SSFM (SCI, XCI and MCI) simulation...")
        ssfm_snr_db_list=\
            SNR_SSFM(lpdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,nspan_dict,
                    ampgain_dict,nf_dict,numstep_dict,acc_dispersion,power_set_dbm,
                    n_sym,rrc_beta,upsratio_elevation,sym_del_margin_ratio,Fs_type,
                    slotBandwidth_GHz,progress_bar_position,is_progress_bar_enabled,result_dir)
        # print("SSFM (SCI, XCI and MCI) simulation completed.")
        ############################################################
        ###########################   SCI   ########################
        ############################################################
        # print("Now starting SSFM (SCI) simulation...")
        ssfm_snr_db_only_sci_list=\
            SNR_SSFM({loi_id:lpdict[loi_id]},loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
                      nspan_dict,ampgain_dict,nf_dict,numstep_dict,acc_dispersion,power_set_dbm,
                      n_sym,rrc_beta,upsratio_elevation,sym_del_margin_ratio,Fs_type,
                      slotBandwidth_GHz,progress_bar_position,0,"")
        # print("SSFM (SCI) simulation completed.")
        
        ssfm_sci_xci_mci_array=10**((power_set_dbm-30-ssfm_snr_db_list)/10)-ASE_variance
        ssfm_sci_array=10**((power_set_dbm-30-ssfm_snr_db_only_sci_list)/10)-ASE_variance
        ssfm_xci_mci_array=ssfm_sci_xci_mci_array-ssfm_sci_array
        #%%#########################################################
        #####################   Result Storage   ###################
        ############################################################
        
        
        # main_result_key=max(model_lpdictdict.keys())
        
        # main_result_exegn_snr_db_list  = exegn_snr_db_list_dict  [main_result_key]
        # main_result_iegn_snr_db_list   = iegn_snr_db_list_dict   [main_result_key]
        # main_result_ign_snr_db_list    = ign_snr_db_list_dict    [main_result_key]
        
        # main_result_exegn_sci_list     = exegn_sci_list_dict     [main_result_key]
        # main_result_iegn_sci_list      = iegn_sci_list_dict      [main_result_key]
        # main_result_ign_sci_list       = ign_sci_list_dict       [main_result_key]
        
        # main_result_exegn_xci_mci_list = exegn_xci_mci_list_dict [main_result_key]
        # main_result_iegn_xci_mci_list  = iegn_xci_mci_list_dict  [main_result_key]
        # main_result_ign_xci_mci_list   = ign_xci_mci_list_dict   [main_result_key]
        completion_date=ctime()
        #%% Result storage
        
        results["inputs"]={
            "alpha":alpha,
            "beta2":beta2,
            "gamma":gamma,
            "lspan":lspan,
            "numspan":numspan,
            # "PT_title"                :PT_title,
            # "TM_title"                :TM_title,
            "power_range_db"          :list(power_range_db),
            "power_set_dbm"           :list(power_set_dbm),
            "rrc_beta"                :rrc_beta,
            "modulationType"          :modulationType,
            # "fiber_type"              :fiber_type,
            "nuOffset"                :float(nuOffset),
            "ampgain"                 :ampgain,
            "nf_LIN"                  :float(nf),
            "nuMCp"                   :int(nuMCp),
            "n_sym"                   :int(n_sym),
            "numstep"                 :int(numstep),
            "sym_del_margin_ratio"    :sym_del_margin_ratio,
            "upsratio_elevation"      :int(upsratio_elevation),
            "Fs_type"                 :Fs_type,
            "loi_id"                  :loi_id,
            "symbolrate"       :symbolrate,
            "bandwidth"       :bandwidth,
            # "numOfGuardSlotsPerDemand":numOfGuardSlotsPerDemand,
            # "span_approx_length_km"   :span_approx_length_km,
            }
        # results["lpdict"]=lpdict
        # results["topology_info"]={
        #     "alpha_dict"     : {f"{i},{j}": alpha_dict   [i,j] for i,j in alpha_dict},
        #     "beta2_dict"     : {f"{i},{j}": beta2_dict   [i,j] for i,j in beta2_dict},
        #     "gamma_dict"     : {f"{i},{j}": gamma_dict   [i,j] for i,j in gamma_dict},
        #     "lspan_dict"     : {f"{i},{j}": lspan_dict   [i,j] for i,j in lspan_dict},
        #     "nspan_dict"     : {f"{i},{j}": nspan_dict   [i,j] for i,j in nspan_dict},
        #     "ampgain_dict"   : {f"{i},{j}": ampgain_dict [i,j] for i,j in ampgain_dict},
        #     "nf_dict"        : {f"{i},{j}": nf_dict      [i,j] for i,j in nf_dict},
        #     "numstep_dict"   : {f"{i},{j}": numstep_dict [i,j] for i,j in numstep_dict},
        #     "acc_dispersion" : acc_dispersion,
        #     }
        results["SNR_info"]={
            "power_set_dbm"          : list(power_set_dbm),
            "ssfm_snr_db_list"       : ssfm_snr_db_list,
            "egn_snr_db_list_dict"  : egn_snr_db_list_dict,
            "ign_snr_db_list_dict"   : ign_snr_db_list_dict,
            }
        results["SCI_XCI_MCI_info"]={
            "power_set_dbm"          : list(power_set_dbm),
            "ssfm_sci_list_dict"     : list(ssfm_sci_array),
            "ssfm_xci_mci_list_dict" : list(ssfm_xci_mci_array),
            "egn_sci_list_dict"     : egn_sci_list_dict,
            "egn_xci_mci_list_dict" : egn_xci_mci_list_dict,
            "ign_sci_list_dict"      : ign_sci_list_dict,
            "ign_xci_mci_list_dict"  : ign_xci_mci_list_dict,
            }
        results["time_tag_info"]={
            "started_at": creation_date,
            "finished_at": completion_date,
            }
        results["results_parent_dir"]=results_parent_dir
        results["result_dir"]=result_dir
        results["err"]=""
        
        # plt.figure()
        # plt.plot(power_set_dbm,ssfm_snr_db_list,"d-",label="SSFM")
        # plt.plot(power_set_dbm,exegn_snr_db_list_dict["0"],"o-",label="Ex. EGN")
        # plt.plot(power_set_dbm,iegn_snr_db_list_dict["0"],"s-",label="iEGN")
        # plt.plot(power_set_dbm,ign_snr_db_list_dict["0"],"^-",label="iGN")
        # plt.legend(fontsize=13)
        # plt.grid("on")
        # plt.title("SSFM SNR vs. Models SNRs")
        # plt.xlabel("Power (dBm)")
        # plt.ylabel("SNR (dB)")
        # plt.savefig(result_dir+"/"+"SNR.png",dpi=200)
        
        # print("skldnsjkadlkasdhjkashdksd")
        # if not single_result["err"]=="":
        #     print(f"ERROR: {single_result['err']}")
        #     continue
        
        # single_result_dir=single_result["result_dir"]
        
        # if not exit_code==0:
        #     print("Flawed result detected. Marking flaw on saved data.")
        #     single_result_dir="[ERROR_ON_EXIT] "+single_result_dir
        
        # try:
        #     mkdir(results_parent_dir)
        # except FileExistsError:
        #     pass
        # # [print(f'python3 EGN_vs_SSFM_Predefined_Network.py "{args}"',end="\n\n") for args in args_list]
        # # raise Exception("Hi----------------")

        # mkdir(result_dir)
        
        for data_name in ['inputs', 'SNR_info', 'SCI_XCI_MCI_info',"time_tag_info"]:
            dump(results[data_name],open(result_dir+"/"+f"{data_name}.json","w+"))
        
        # if not exit_code==0:
        #     dump(str(results["err"]),open(results+"/"+"error_log.json","w+"))
        
        #%%
    except Exception as err:
        raise Exception(err)
        results["err"]=str(err)
    
    # print("asas")
    # print("\f")
    # print(results["inputs"])
    # print(results["lpdict"])
    # print(results["topology_info"])
    # print(results["SNR_info"])
    # system("cls")
    
    # print(results)
    
    # print("3434")