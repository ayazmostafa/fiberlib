# -*- coding: utf-8 -*-
"""
Created on Tue Dec 27 20:19:55 2022

@author: Mostafa
"""
# from subprocess import Popen,PIPE
# from asyncio import subprocess
from RWALib import Elastic_RWA,importPhysicalTopology,importTrafficMatrix
# from json import loads,dump
from numpy.random import randint
# from os import mkdir
import subprocess
from TopologyAndTrafficManager import createRandomHash
#%%
if __name__=="__main__":
    
    ##########################################################################
    ##########################################################################
    #########################                       ##########################
    ######################### Adjustable parameters ##########################
    #########################                       ##########################
    ##########################################################################
    ##########################################################################
    
    chBandwidth_choices=[50e9,33e9]
    symbolRate_choices=[32e9]
    
    num_lp_per_nu_choices=[3,4]
    numlp_choices=[7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
    numch_choices=[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
    numlinks_choices=[5]
    nspan_choices=[1,2,3,4,5]
    lspan_candidate_list_choices=[
        [[100e3],[1]]]
    # lspan_candidate_list_choices=[
        # [[100e3],[1]],[[60e3,80e3,100e3,120e3,140e3],[0.2,0.2,0.2,0.2,0.2]]]
    fiber_type_choices=["SMF","NZ-DSF"]
    
    #########################################
    
    num_of_process_clusters=1e9
    
    num_of_parallel_processes=8
    
    # Topology and traffic information
    # PT_TM_import_info=[
    #     # ("Backbone_Data","Test1_PT","Test1_TM"),
    #     ("Backbone_Data","US_PhysicalTopology","US_Total_TrafficMatrix"),
    #     # ("Backbone_Data","EU_PhysicalTopology","EU_Total_TrafficMatrix"),
    #     # ("Backbone_Data","GER_PhysicalTopology","GER_Total_TrafficMatrix"),
    #     ]
    
    # # US chosen LOI IDs (Lightpath link number >= 4)
    # chosen_loi_ids=['dem3']
    
    # # EU chosen LOI IDs (Lightpath link number >= 8)
    # chosen_loi_ids=['dem57', 'dem72', 'dem76', 'dem79', 'dem84', 'dem86', 'dem94', 'dem96', 'dem139', 'dem157',
    #     'dem160', 'dem204', 'dem355', 'dem373', 'dem376', 'dem400', 'dem409', 'dem413', 'dem423', 'dem427',
    #     'dem430', 'dem454', 'dem489', 'dem502', 'dem597', 'dem600', 'dem608', 'dem609', 'dem610', 'dem611',
    #     'dem678', 'dem681', 'dem689', 'dem691']
    
    # # GER chosen LOI IDs (Lightpath link number >= 5)
    # chosen_loi_ids=['dem29', 'dem30', 'dem44', 'dem45', 'dem59', 'dem60', 'dem68', 'dem72', 'dem74', 'dem75',
    #     'dem105','dem106', 'dem121', 'dem126', 'dem151', 'dem186', 'dem214', 'dem215', 'dem216', 'dem217',
    #     'dem219','dem229', 'dem230', 'dem231', 'dem232', 'dem234', 'dem235', 'dem237']
    ##########################################################################
    ##########################################################################
    #####################                              #######################
    ##################### End of adjustable parameters #######################
    #####################                              #######################
    ##########################################################################
    ##########################################################################
    
    
    # for PT_TM_DB_dir,PT_title,TM_title in PT_TM_import_info:
    for _ in range(int(num_of_process_clusters)):
        
        processes = []
        process_ids = []
        args_list=[]
        
        for __ in range(int(num_of_parallel_processes)):
        
            chBandwidth = chBandwidth_choices[int(randint(0,len(chBandwidth_choices)))]
            symbolRate = symbolRate_choices[int(randint(0,len(symbolRate_choices)))]
            num_lp_per_nu = num_lp_per_nu_choices[int(randint(0,len(num_lp_per_nu_choices)))]
            numlp = numlp_choices[int(randint(0,len(numlp_choices)))]
            numch = numch_choices[int(randint(0,len(numch_choices)))]
            numlinks = numlinks_choices[int(randint(0,len(numlinks_choices)))]
            nspan = nspan_choices[int(randint(0,len(nspan_choices)))]
            lspan_candidate_list = lspan_candidate_list_choices[int(randint(0,len(lspan_candidate_list_choices)))]
            fiber_type = fiber_type_choices[int(randint(0,len(fiber_type_choices)))]
        
            SNR_script = 'Elsevier_EGN_vs_SSFM_Predefined_Network.py'
            
            process_id=createRandomHash(hashlength=20)
            
            processes.append(subprocess.Popen(["python",SNR_script,process_id,str(chBandwidth),
                str(symbolRate),str(num_lp_per_nu),str(numlp),str(numch),str(numlinks),str(nspan),
                ",".join([str(u) for u in lspan_candidate_list[0]]),
                ",".join([str(u) for u in lspan_candidate_list[1]]),fiber_type]))
            process_ids.append(process_id)
    
        exit_codes = [process.wait() for process in processes]
        
        for process_id,exit_code in zip(process_ids,exit_codes):
            if not exit_code==0:
                print(f"Process {process_id} exited with error.")
        
        # if not any(exit_codes):
        #     print("No errors occured.")
    
    # results=[process.stdout.read().decode().strip().replace("'","\"") for process in processes]
    
    # results=[loads(process.stdout.read().decode().strip().replace("'","\"")) for process in processes]
    
    #%%
    # for single_result,exit_code in zip(results,exit_codes):
        
    #     if not single_result["err"]=="":
    #         print(f"ERROR: {single_result['err']}")
    #         continue
        
    #     single_result_dir=single_result["result_dir"]
        
    #     if not exit_code==0:
    #         print("Flawed result detected. Marking flaw on saved data.")
    #         single_result_dir="[ERROR_ON_EXIT] "+single_result_dir
        
    #     try:
    #         mkdir(single_result["results_parent_dir"])
    #     except FileExistsError:
    #         pass
    #     # [print(f'python3 EGN_vs_SSFM_Predefined_Network.py "{args}"',end="\n\n") for args in args_list]
    #     # raise Exception("Hi----------------")

    #     mkdir(single_result_dir)
        
    #     for data_name in ['inputs', 'SNR_info', 'SCI_XCI_MCI_info']:
    #         dump(single_result[data_name],open(single_result_dir+"/"+f"{data_name}.json","w+"))
        
    #     if not exit_code==0:
    #         dump(str(single_result["err"]),open(single_result_dir+"/"+"error_log.json","w+"))