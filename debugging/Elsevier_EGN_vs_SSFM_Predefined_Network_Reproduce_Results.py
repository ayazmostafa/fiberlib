# -*- coding: utf-8 -*-
"""
Created on Tue May 31 12:43:23 2022

@author: smosaya
"""

from SSFM import SNR as SNR_SSFM#,lpdictplot,elastic_lpdictplot
from NLI_modeling import SNR_per_num_side_channels as SNR_EGN_per_num_side_channels,\
    SNR_per_num_side_channels_Elsevier as SNR_EGN_per_num_side_channels_Elsevier
# from NLI_modeling_multiprocessing import SNR_per_num_side_channels as SNR_EGN_per_num_side_channels
from time import ctime
from os import mkdir,rename
from json import dump,load
# from json import loads
from TopologyAndTrafficManager import randomSimulation,setUpSpanPropertiesAccordingToTheLOI
from sys import argv
# import matplotlib.pyplot as plt
# from os import system
from numpy.random import uniform
from TopologyAndTrafficManager import createRandomHash
import numpy as np
#%%
if __name__=="__main__":
    
    # # Variables
    # process_id="P"+str(uniform(1000000))
    # chBandwidth=33e9
    # symbolRate=32e9
    
    # num_lp_per_nu=3
    # numlp=14
    # numch=7
    # numlinks=10
    # nspan=1
    # lspan_candidate_list=[[100e3],[1]]
    # fiber_type="SMF"
    
    # General parameters
    # power_range_db=[0] # Range of power around the optimum power. Set it to [0] for optimum power of maximum SNR.
    # rrc_beta=0.02 # Root-raised cosine roll-off factor.
    
    # # NLI-modeling parameters
    # nuMCp=1e5 # Number of Monte-Carlo integration points. Keep it high to avoid NaN-valued SNRs.
    
    # # SSFM parameters
    # numstep=200 # Number of fiber sections per optical fiber.
    # n_sym=1.5e5 # Number of symbols at transmitter.
    # upsratio_elevation=2 # Upsampling ratio elevation.
    # sym_del_margin_ratio=4000/n_sym # Number of expendable marginal symbols.
    # Fs_type="total" # Sampling frequency type.
    
    # # Constants
    # modulationType="cube4_16"
    # nuOffset=190e12
    
    # ampgain="FC" # "FC" for fully-compensated
    
    # nf=10**0.55
    # loi_id="dem1"
    
    results_parent_dir="Results_Set_Elsevier_1"
    
    creation_date=ctime()
    
    process_id               = createRandomHash(20)
    # chBandwidth = float(argv[2])
    # symbolRate = float(argv[3])
    # num_lp_per_nu = int(argv[4])
    # numlp = int(argv[5])
    # numch = int(argv[6])
    # numlinks = int(argv[7])
    # nspan = int(argv[8])
    # lspan_candidate_list = [
    #     [float(u) for u in argv[9].split(",")],
    #     [float(u) for u in argv[10].split(",")]
    #     ]
    # # lspan_candidate_list = argv[10]
    # fiber_type = argv[11]
    
    is_progress_bar_enabled=1
    
    
    # Apply RWA and determine the lightpaths.
    # lpdict,lspan_dict,nspan_dict=randomSimulation(lspan_candidate_list=lspan_candidate_list,nspan=nspan,
    #     num_lp_per_nu=num_lp_per_nu,nuOffset=nuOffset,numlinks=numlinks,numch=numch,numlp=numlp,
    #     symbolRate=symbolRate,chBandwidth=chBandwidth,modulationType=modulationType,_type_="optical-bus")
    
    
    # Result reproduction
    # rep_result_folder=r"D:\Important_Docs\fiberlib\Storage\__pycache__\New_Results_Modified_Prop_3_SMF_4\Thu Dec 15 16.35.35 2022 #XGBhG53f5Q (Good on IGN) (Good on IEGN)".replace("\\", "/")
    rep_result_folder=r"D:\Important_Docs\fiberlib\Storage\Important_Results\Results_NZ-DSF_Random_km\Sat Nov  5 22.44.27 2022 #ck1swAWRWb (Good on IGN) (Good on IEGN)".replace("\\", "/")
    
    
    lpdict=load(open(rep_result_folder+"/lpdict.json","r"))
    topology_info=load(open(rep_result_folder+"/topology_info.json","r"))
    inputs=load(open(rep_result_folder+"/inputs.json","r"))
    
    lspan_dict={tuple(key.split(",")):val for key,val in topology_info["lspan_dict"].items()}
    nspan_dict={tuple(key.split(",")):val for key,val in topology_info["nspan_dict"].items()}
    
    power_range_db           = [-2,-1,0,1,2]
    # power_set_dbm            = inputs['power_set_dbm']
    rrc_beta                 = inputs['rrc_beta']
    # numlinks                 = inputs['numlinks']
    chBandwidth              = inputs['chBandwidth']
    symbolRate               = inputs['symbolRate']
    # modulationType           = inputs['modulationType']
    # numlp                    = inputs['numlp']
    # num_lp_per_nu            = inputs['num_lp_per_nu']
    # numch                    = inputs['numch']
    # lspan_candidate_list     = inputs['lspan_candidate_list']
    # nspan                    = inputs['nspan']
    fiber_type               = inputs['fiber_type']
    # nuOffset                 = inputs['nuOffset']
    # max_num_out_of_bus_nodes = inputs['max_num_out_of_bus_nodes']
    ampgain                  = inputs['ampgain']
    nf                       = inputs['nf_LIN']
    nuMCp                    = 1e5
    n_sym                    = 4e5
    numstep                  = 200
    sym_del_margin_ratio     = 6000/n_sym
    upsratio_elevation       = 2
    Fs_type                  = "total"
    loi_id                   = "dem1"


    progress_bar_position=int(loi_id[3:])
# else:
    
    
    # print(lpdict)
# else:
    # # Remoooooooooove this
    # lpdict={loi_id:lpdict[loi_id]}
    
    results={}
    
    result_dir=results_parent_dir+"/"+creation_date.replace(':','.')+" #"+process_id+" reproduced-from #"+rep_result_folder.split("#")[1]
    # result_dir=results_parent_dir+"/"+process_id
# else:
    # print(f"The lightpath ID is {loi_id}.")
    # print(f"The lightpath is {lpdict[loi_id]['nodeList']}.")
    # print(f"The lightpath length is {sum([sum(lspan_dict[i,j]) for i,j in zip(lpdict[loi_id]['nodeList'],lpdict[loi_id]['nodeList'][1:])])}km.")
    # print("################################################################################")
    # print(f"Results parent directory = {results_parent_dir}")
    # print(f"The results will be saved in {result_dir}.")
    
    # try:
        
    alpha_dict,beta2_dict,gamma_dict,nf_dict,ampgain_dict,numstep_dict,acc_dispersion=\
    setUpSpanPropertiesAccordingToTheLOI(lpdict,loi_id,fiber_type,nf,ampgain,numstep,lspan_dict,nspan_dict)
    
    ampgain_dict={key:list(np.exp(np.array(lspan_dict[key])*alpha_dict[key])) for key in lspan_dict}
    
    model_lpdictdict={"0":lpdict}
    
    try:
        mkdir(results_parent_dir)
    except FileExistsError:
        pass
    # [print(f'python3 EGN_vs_SSFM_Predefined_Network.py "{args}"',end="\n\n") for args in args_list]
    # raise Exception("Hi----------------")
    
    mkdir(result_dir)
    #%%#########################################################
    ############################################################
    #######################              #######################
    #######################     EGN      #######################
    #######################              #######################
    ############################################################
    ############################################################
    
    ############################################################
    ################# Limited neighbor channels ################
    ############################################################
    # print("Now starting model simulation...")
    power_set_dbm,exegn_elsevier_snr_db_list_dict,iegn_snr_db_list_dict,ign_snr_db_list_dict,\
        exegn_elsevier_sci_list_dict,iegn_sci_list_dict,ign_sci_list_dict,exegn_elsevier_xci_mci_list_dict,\
        iegn_xci_mci_list_dict,ign_xci_mci_list_dict,ASE_variance=SNR_EGN_per_num_side_channels_Elsevier(lpdict,
        model_lpdictdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,ampgain_dict,nf_dict,
        power_range_db,nuMCp,progress_bar_position,is_progress_bar_enabled,result_dir)
    # print("Model simulation completed.")
    #%%#########################################################
    ############################################################
    #######################               ######################
    #######################     SSFM      ######################
    #######################               ######################
    ############################################################
    ############################################################
    
    ############################################################
    #####################   SCI + XCI + MCI   ##################
    ############################################################
    # print("Now starting SSFM (SCI, XCI and MCI) simulation...")
    ssfm_snr_db_list=\
        SNR_SSFM(lpdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,nspan_dict,
                ampgain_dict,nf_dict,numstep_dict,acc_dispersion,power_set_dbm,
                n_sym,rrc_beta,upsratio_elevation,sym_del_margin_ratio,Fs_type,
                symbolRate/1e9,progress_bar_position,is_progress_bar_enabled,result_dir)
    # print("SSFM (SCI, XCI and MCI) simulation completed.")
    ############################################################
    ###########################   SCI   ########################
    ############################################################
    # print("Now starting SSFM (SCI) simulation...")
    ssfm_snr_db_only_sci_list=\
        SNR_SSFM({loi_id:lpdict[loi_id]},loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
                  nspan_dict,ampgain_dict,nf_dict,numstep_dict,acc_dispersion,power_set_dbm,
                  n_sym,rrc_beta,upsratio_elevation,sym_del_margin_ratio,Fs_type,
                  symbolRate/1e9,progress_bar_position,is_progress_bar_enabled,result_dir)
    # print("SSFM (SCI) simulation completed.")
    
    ssfm_sci_xci_mci_array=10**((power_set_dbm-30-ssfm_snr_db_list)/10)-ASE_variance
    ssfm_sci_array=10**((power_set_dbm-30-ssfm_snr_db_only_sci_list)/10)-ASE_variance
    ssfm_xci_mci_array=ssfm_sci_xci_mci_array-ssfm_sci_array
    #%%#########################################################
    #####################   Result Storage   ###################
    ############################################################
    
    
    # main_result_key=max(model_lpdictdict.keys())
    
    # main_result_exegn_snr_db_list  = exegn_snr_db_list_dict  [main_result_key]
    # main_result_iegn_snr_db_list   = iegn_snr_db_list_dict   [main_result_key]
    # main_result_ign_snr_db_list    = ign_snr_db_list_dict    [main_result_key]
    
    # main_result_exegn_sci_list     = exegn_sci_list_dict     [main_result_key]
    # main_result_iegn_sci_list      = iegn_sci_list_dict      [main_result_key]
    # main_result_ign_sci_list       = ign_sci_list_dict       [main_result_key]
    
    # main_result_exegn_xci_mci_list = exegn_xci_mci_list_dict [main_result_key]
    # main_result_iegn_xci_mci_list  = iegn_xci_mci_list_dict  [main_result_key]
    # main_result_ign_xci_mci_list   = ign_xci_mci_list_dict   [main_result_key]
    completion_date=ctime()
    #%% Result storage
    
    # results["inputs"]={
    #     # "PT_title"                :PT_title,
    #     # "TM_title"                :TM_title,
    #     "power_range_db"          :list(power_range_db),
    #     "power_set_dbm"           :list(power_set_dbm),
    #     "rrc_beta"                :rrc_beta,
    #     "modulationType"          :modulationType,
    #     "fiber_type"              :fiber_type,
    #     "nuOffset"                :float(nuOffset),
    #     "ampgain"                 :ampgain,
    #     "nf_LIN"                  :float(nf),
    #     "nuMCp"                   :int(nuMCp),
    #     "n_sym"                   :int(n_sym),
    #     "numstep"                 :int(numstep),
    #     "sym_del_margin_ratio"    :sym_del_margin_ratio,
    #     "upsratio_elevation"      :int(upsratio_elevation),
    #     "Fs_type"                 :Fs_type,
    #     "loi_id"                  :loi_id,
    #     "chBandwidth"       :chBandwidth,
    #     "symbolRate": symbolRate,
    #     # "numOfGuardSlotsPerDemand":numOfGuardSlotsPerDemand,
    #     "lspan_candidate_list"   :lspan_candidate_list,
    #     }
    # results["topology_info"]={
    #     "alpha_dict"     : {f"{i},{j}": alpha_dict   [i,j] for i,j in alpha_dict},
    #     "beta2_dict"     : {f"{i},{j}": beta2_dict   [i,j] for i,j in beta2_dict},
    #     "gamma_dict"     : {f"{i},{j}": gamma_dict   [i,j] for i,j in gamma_dict},
    #     "lspan_dict"     : {f"{i},{j}": lspan_dict   [i,j] for i,j in lspan_dict},
    #     "nspan_dict"     : {f"{i},{j}": nspan_dict   [i,j] for i,j in nspan_dict},
    #     "ampgain_dict"   : {f"{i},{j}": ampgain_dict [i,j] for i,j in ampgain_dict},
    #     "nf_dict"        : {f"{i},{j}": nf_dict      [i,j] for i,j in nf_dict},
    #     "numstep_dict"   : {f"{i},{j}": numstep_dict [i,j] for i,j in numstep_dict},
    #     "acc_dispersion" : acc_dispersion,
    #     }
    results["SNR_info"]={
        "power_set_dbm"          : list(power_set_dbm),
        "ssfm_snr_db_list"       : ssfm_snr_db_list,
        "exegn_elsevier_snr_db_list_dict" : exegn_elsevier_snr_db_list_dict,
        "iegn_snr_db_list_dict"  : iegn_snr_db_list_dict,
        "ign_snr_db_list_dict"   : ign_snr_db_list_dict,
        }
    results["SCI_XCI_MCI_info"]={
        "power_set_dbm"          : list(power_set_dbm),
        "ssfm_sci_list_dict"     : list(ssfm_sci_array),
        "ssfm_xci_mci_list_dict" : list(ssfm_xci_mci_array),
        "exegn_elsevier_sci_list_dict"    : exegn_elsevier_sci_list_dict,
        "exegn_elsevier_xci_mci_list_dict": exegn_elsevier_xci_mci_list_dict,
        "iegn_sci_list_dict"     : iegn_sci_list_dict,
        "iegn_xci_mci_list_dict" : iegn_xci_mci_list_dict,
        "ign_sci_list_dict"      : ign_sci_list_dict,
        "ign_xci_mci_list_dict"  : ign_xci_mci_list_dict,
        }
    results["time_tag_info"]={
        "started_at": creation_date,
        "finished_at": completion_date,
        }
    results["lpdict_info"]=lpdict
    results["lspan_dict_info"]={f"{i},{j}": list(lspan_dict[i,j]) for i,j in lspan_dict}
    results["nspan_dict_info"]={f"{i},{j}": int(nspan_dict[i,j]) for i,j in nspan_dict}
    results["results_parent_dir"]=results_parent_dir
    results["result_dir"]=result_dir
    results["err"]=""
    
    # plt.figure()
    # plt.plot(power_set_dbm,ssfm_snr_db_list,"d-",label="SSFM")
    # plt.plot(power_set_dbm,exegn_snr_db_list_dict["0"],"o-",label="Ex. EGN")
    # plt.plot(power_set_dbm,iegn_snr_db_list_dict["0"],"s-",label="iEGN")
    # plt.plot(power_set_dbm,ign_snr_db_list_dict["0"],"^-",label="iGN")
    # plt.legend(fontsize=13)
    # plt.grid("on")
    # plt.title("SSFM SNR vs. Models SNRs")
    # plt.xlabel("Power (dBm)")
    # plt.ylabel("SNR (dB)")
    # plt.savefig(result_dir+"/"+"SNR.png",dpi=200)
    
    # print("skldnsjkadlkasdhjkashdksd")
    # if not single_result["err"]=="":
    #     print(f"ERROR: {single_result['err']}")
    #     continue
    
    # single_result_dir=single_result["result_dir"]
    
    # if not exit_code==0:
    #     print("Flawed result detected. Marking flaw on saved data.")
    #     single_result_dir="[ERROR_ON_EXIT] "+single_result_dir
    
    # try:
    #     mkdir(results_parent_dir)
    # except FileExistsError:
    #     pass
    # # [print(f'python3 EGN_vs_SSFM_Predefined_Network.py "{args}"',end="\n\n") for args in args_list]
    # # raise Exception("Hi----------------")

    # mkdir(result_dir)
    
    for data_name in results:
        if data_name not in ["results_parent_dir","results_dir","err"]:
            dump(results[data_name],open(result_dir+"/"+f"{data_name}.json","w+"))
    
    rename(results_parent_dir+"/"+creation_date.replace(':','.')+" #"+process_id+" reproduced-from #"+rep_result_folder.split("#")[1],
           results_parent_dir+"/[Finished] "+creation_date.replace(':','.')+" #"+process_id+" reproduced-from #"+rep_result_folder.split("#")[1])
    # if not exit_code==0:
    #     dump(str(results["err"]),open(results+"/"+"error_log.json","w+"))
    
    #%%
    # except Exception as err:
    #     raise Exception(err)
    #     results["err"]=str(err)
    
    # print("asas")
    # print("\f")
    # print(results["inputs"])
    # print(results["lpdict"])
    # print(results["topology_info"])
    # print(results["SNR_info"])
    # system("cls")
    
    # print(results)
    
    # print("3434")