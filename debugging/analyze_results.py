






import numpy as np
import matplotlib.pyplot as plt
import json
import os
from RWALib import importTrafficMatrix,importPhysicalTopology,Elastic_RWA
#%%
# def calcImprovments():
    
#%%
def elastic_lpdictplot(lpdict,loi_id):
    
    loi_node_list=lpdict[loi_id]["nodeList"]
    loi_link_list=list(zip(loi_node_list,loi_node_list[1:]))
    
    loi_link_lpid_dict={(i,j): [lpid for lpid,lp in lpdict.items() if (i,j) in zip(lp["nodeList"],lp["nodeList"][1:])] for i,j in loi_link_list}
    
    loi_link_lp_draws={}
    for i,j in loi_link_list:
        loi_link_lp_draws[i,j]={}
        max_link_freq=max([lpdict[lpid]["nu"]+lpdict[lpid]["chBandwidth"]/2 for lpid in loi_link_lpid_dict[i,j]])
        min_link_freq=min([lpdict[lpid]["nu"]-lpdict[lpid]["chBandwidth"]/2 for lpid in loi_link_lpid_dict[i,j]])
        freq_array=np.linspace(min_link_freq,max_link_freq,10000)
        loi_link_lp_draws[i,j]["freq_array"]=freq_array
        loi_link_lp_draws[i,j]["lps"]={}
        for lpid in loi_link_lpid_dict[i,j]:
            loi_link_lp_draws[i,j]["lps"][lpid]=np.cos(np.pi*(freq_array-lpdict[lpid]["nu"])/lpdict[lpid]["chBandwidth"])\
                *(freq_array>=lpdict[lpid]["nu"]-lpdict[lpid]["chBandwidth"]/2)\
                    *(freq_array<=lpdict[lpid]["nu"]+lpdict[lpid]["chBandwidth"]/2)
        
    return loi_link_lpid_dict,loi_link_list,loi_link_lp_draws
#%%

if __name__=="__main__":
    
    parent_folder="Analyzed"
    
    try:
        os.mkdir(parent_folder)
    except:
        pass
    
    x1111=importTrafficMatrix("US_Total_TrafficMatrix","Backbone_Data")
    y=importPhysicalTopology("US_PhysicalTopology","Backbone_Data")
    
    lpdict=Elastic_RWA(y[0],y[1],x1111,slotBandwidth_GHz=6.25,
                       modulationType="cube4_16",nuOffset=190e12,
                       numOfGuardSlotsPerDemand=1,
                       numOfRWAResults=1,maxNumOfCandidatePaths=1)[0]
# else:    
    """Determining link lengths"""
    span_approx_length_km=125
    lspan_dict={}
    nspan_dict={}
    for i,j in y[1]:
        nspan=int(np.ceil(y[1][i,j]/span_approx_length_km))
        # lspan=link_length_km_dict[i,j]/nspan
        lspan_dict[i,j]=[span_approx_length_km*1e3]*(nspan-1)+[(y[1][i,j]-span_approx_length_km*(nspan-1))*1e3]
        nspan_dict[i,j]=nspan
    
    
    inputs_list=[]
    snrs_list=[]
    
    
    for x in list(os.walk("Results_Set_Test2_Progress_bar_enabled"))[0][1]:
        
        snrs_list.append(json.load(open(f"Results_Set_Test2_Progress_bar_enabled/{x}/SNR_info.json","r")))
        inputs_list.append(json.load(open(f"Results_Set_Test2_Progress_bar_enabled/{x}/inputs.json","r")))
    
    
    ssfms=[u["ssfm_snr_db_list"] for u in snrs_list]
    exegns=[u["exegn_snr_db_list_dict"]["0"] for u in snrs_list]
    iegns=[u["iegn_snr_db_list_dict"]["0"] for u in snrs_list]
    igns=[u["ign_snr_db_list_dict"]["0"] for u in snrs_list]
    
    loi_ids=[u["loi_id"] for u in inputs_list]
    
    plt.figure()
    plt.plot(loi_ids,ssfms)
    plt.plot(loi_ids,exegns)
    plt.plot(loi_ids,iegns)
    plt.plot(loi_ids,igns)
    plt.xlabel("Demand ID")
    plt.ylabel("SNR (dB)")
    plt.title("SNRs of multiple demands of the US backbone\nnetwork at optimum power")
    plt.grid("on")
    plt.xticks(rotation=-45)
    plt.savefig(parent_folder+"/SNR_raw.png",dpi=200)
    
    plt.figure()
    plt.plot(loi_ids,abs(np.array(exegns)-ssfms),label=r"$|\mathrm{SNR}_\mathrm{XEGN}-\mathrm{SNR}_\mathrm{SSFM}|$")
    plt.plot(loi_ids,abs(np.array(iegns)-ssfms),label=r"$|\mathrm{SNR}_\mathrm{iEGN}-\mathrm{SNR}_\mathrm{SSFM}|$")
    plt.plot(loi_ids,abs(np.array(igns)-ssfms),label=r"$|\mathrm{SNR}_\mathrm{iGN}-\mathrm{SNR}_\mathrm{SSFM}|$")
    plt.legend(fontsize=10)
    plt.xticks(rotation=-45)
    plt.xlabel("Demand ID")
    plt.ylabel("SNR (dB)")
    plt.title("Absolute value of SNR differences per demands\nof the US backbone network at optimum power")
    plt.grid("on")
    plt.savefig(parent_folder+"/SNR_diff_raw.png",dpi=200)
    
    plt.figure()
    plt.plot(loi_ids,np.array([sum([sum(lspan_dict[i,j]) for i,j in zip(lpdict[loi_id]["nodeList"],lpdict[loi_id]["nodeList"][1:])]) for loi_id in loi_ids])/1e3)
    plt.legend(fontsize=10)
    plt.xticks(rotation=-45)
    plt.xlabel("Demand ID")
    plt.ylabel("Lightpath length (km)")
    plt.title("Lightpaths lengths (km)")
    plt.grid("on")
    plt.savefig(parent_folder+"/lp_length_raw.png",dpi=200)
    # plt.plot(abs(np.array(exegns)-ssfms))
    
    good_lois=np.array(loi_ids)[((abs(np.array(exegns)-ssfms)<abs(np.array(iegns)-ssfms))*(abs(np.array(exegns)-ssfms)<abs(np.array(igns)-ssfms))).T[0]]
    ssfms_at_good_lois=np.array(ssfms)[((abs(np.array(exegns)-ssfms)<abs(np.array(iegns)-ssfms))*(abs(np.array(exegns)-ssfms)<abs(np.array(igns)-ssfms))).T[0]]
    exegns_at_good_lois=np.array(exegns)[((abs(np.array(exegns)-ssfms)<abs(np.array(iegns)-ssfms))*(abs(np.array(exegns)-ssfms)<abs(np.array(igns)-ssfms))).T[0]]
    iegns_at_good_lois=np.array(iegns)[((abs(np.array(exegns)-ssfms)<abs(np.array(iegns)-ssfms))*(abs(np.array(exegns)-ssfms)<abs(np.array(igns)-ssfms))).T[0]]
    igns_at_good_lois=np.array(igns)[((abs(np.array(exegns)-ssfms)<abs(np.array(iegns)-ssfms))*(abs(np.array(exegns)-ssfms)<abs(np.array(igns)-ssfms))).T[0]]
    
    #%%
    # loi_id="dem4"
    # _,loi_link_list,lp_d=elastic_lpdictplot(lpdict,loi_id)
    
    # for i,j in loi_link_list:
    #     plt.figure()
    #     plt.plot(lp_d[i,j]["freq_array"],sum(lp_d[i,j]["lps"].values()))
    #     plt.plot([lpdict[loi_id]["nu"],lpdict[loi_id]["nu"]],[0,1],"k:")
    #%%
    plt.figure()
    plt.plot(good_lois,ssfms_at_good_lois)
    plt.plot(good_lois,exegns_at_good_lois)
    plt.plot(good_lois,iegns_at_good_lois)
    plt.plot(good_lois,igns_at_good_lois)
    plt.xlabel("Demand ID")
    plt.ylabel("SNR (dB)")
    plt.title("SNRs of multiple demands of the US backbone\nnetwork at optimum power for good LOIs")
    plt.grid("on")
    plt.xticks(rotation=-45)
    plt.savefig(parent_folder+"/SNR_good.png",dpi=200)
    
    plt.figure()
    plt.plot(good_lois,abs(np.array(exegns_at_good_lois)-ssfms_at_good_lois))
    plt.plot(good_lois,abs(np.array(iegns_at_good_lois)-ssfms_at_good_lois))
    plt.plot(good_lois,abs(np.array(igns_at_good_lois)-ssfms_at_good_lois))
    plt.legend(fontsize=10)
    plt.xticks(rotation=-45)
    plt.xlabel("Demand ID")
    plt.ylabel("SNR (dB)")
    plt.title("Absolute value of SNR differences per demands of the\nUS backbone network at optimum power for good LOIs")
    plt.grid("on")
    plt.savefig(parent_folder+"/SNR_diff_good.png",dpi=200)
    
    plt.figure()
    plt.plot(good_lois,np.array([sum([sum(lspan_dict[i,j]) for i,j in zip(lpdict[loi_id]["nodeList"],lpdict[loi_id]["nodeList"][1:])]) for loi_id in good_lois])/1e3)
    plt.legend(fontsize=10)
    plt.xticks(rotation=-45)
    plt.xlabel("Demand ID")
    plt.ylabel("Lightpath length (km)")
    plt.title("Lightpaths lengths (km)")
    plt.grid("on")
    plt.savefig(parent_folder+"/lp_length_good.png",dpi=200)
    
    # loi_id="dem4"
    # _,loi_link_list,lp_d=elastic_lpdictplot(lpdict,loi_id)
    
    for loi_id in good_lois:
        _,loi_link_list,lp_d=elastic_lpdictplot(lpdict,loi_id)
        try:
            os.mkdir(parent_folder+"/"+loi_id)
        except:
            pass
        for i,j in loi_link_list:
            plt.figure()
            plt.plot((lp_d[i,j]["freq_array"]-190e12)/1e9,sum(lp_d[i,j]["lps"].values()))
            plt.plot(np.array([lpdict[loi_id]["nu"],lpdict[loi_id]["nu"]])/1e9-190e3,[0,1],"k:")
            plt.title(f"Spectrum of link {i}-{j} of lightpath {loi_id}")
            plt.xlabel("Frequency (GHz)")
            # plt.plot()
            # plt.ylabel("")
            # plt.grid("on")
            plt.savefig(parent_folder+"/"+loi_id+f"/link_spec_{i}_{j}.png",dpi=200)
            plt.close()