# -*- coding: utf-8 -*-
"""
Created on Sat Feb  5 01:29:26 2022

@author: M
"""

# import matplotlib.pyplot as plt
from scipy.signal import resample_poly
from numpy.fft import fft,ifft
# from tqdm import tqdm
from numpy import pi,sin,cos,exp,ceil,real,imag,kron,convolve,\
array,linspace,arange,floor,zeros,log10,complex128 as npcomplex128,isnan,trace
from numpy.random import randn,randint
from numpy.linalg import inv,norm
from json import load as jsonload
# from TopologyAndTrafficManager import randomSimulation
# IMPORTANT
# Note that upsratio is the sampling frequency divided by double times the LOI symbol rate, not LOI symbol rate!

# plt.close('all')
# print('\f')
# print('='*50)
# print('In the current version of the code, the symbol rates and bandwidths of'+\
#       ' all the lightpath transmitters are equal.')
# print('fsampling can vary in links, so do time_D, freq_D, upsratio and signal length.')
# print('IMPORTANT: constant fsampling works properly at network level.')
# print('IMPORTANT: variable fsampling DOES NOT work properly at network level.')
# print('='*50)
MU=500
#%%
# def plotch(center_nu,bandwidth,amp=1):
#     temp_f=linspace(center_nu-bandwidth/2,center_nu+bandwidth/2,10)
#     plt.plot(temp_f,amp*sin(pi/bandwidth*(temp_f-center_nu+bandwidth/2)))
# #%%
# def scatter_plot(x,col='k',markersize=0.7,label='none'):
#     plt.plot(real(x),imag(x),col+'.',markersize=markersize,label=label)
#     plt.axis("equal")
# #%%
# def lpdictplot(lpdict,loi_id,bw,hgap=1,vgap=0.3,lvgap=0.05):
    
#     loi_node_list=lpdict[loi_id]['nodeList']
#     loi_link_list=list(zip(loi_node_list,loi_node_list[1:]))
    
#     loi_node_coords={loi_node_list[i]:hgap*i for i in range(len(loi_node_list))}
    
#     """Plot the LOI."""
#     plt.figure()
#     for i,j in loi_link_list:
#         plt.plot([loi_node_coords[i],loi_node_coords[j]],[0,0],'ko-')
    
#     """Plot the neighbor lightpaths."""
#     for demid in lpdict:
        
#         if demid==loi_id:
#             continue
        
#         ind=(lpdict[demid]['nu']-lpdict[loi_id]['nu'])/bw
#         lpnodes=lpdict[demid]['nodeList']
        
#         lpnodes_copy=lpnodes.copy()
        
#         lplinks_in_common=list(filter(lambda link: link in loi_link_list,list(zip(lpnodes_copy,lpnodes_copy[1:]))))
        
#         if len(lplinks_in_common)>0:
#             plt.plot([loi_node_coords[lplinks_in_common[0][0]],loi_node_coords[lplinks_in_common[-1][-1]]],[ind*vgap]*2,'o-')
#             plt.text(sum([loi_node_coords[lplinks_in_common[0][0]],loi_node_coords[lplinks_in_common[-1][-1]]])/2,ind*vgap+lvgap,demid,ha='center')
#             plt.text(sum([loi_node_coords[lplinks_in_common[0][0]],loi_node_coords[lplinks_in_common[-1][-1]]])/2,ind*vgap-2.5*lvgap,f"nu={lpdict[demid]['nu']/lpdict[demid]['chBandwidth']}",ha='center')
#             # plt.plot([loi_node_coords[lpnodes[0]],loi_node_coords[lpnodes[-1]]],[ind*vgap]*2,'o-')
#             # plt.text(sum([loi_node_coords[lpnodes[0]],loi_node_coords[lpnodes[-1]]])/2,ind*vgap+lvgap,demid,ha='center')
#             # plt.text(sum([loi_node_coords[lpnodes[0]],loi_node_coords[lpnodes[-1]]])/2,ind*vgap-2.5*lvgap,f"nu={lpdict[demid]['nu']/lpdict[demid]['chBandwidth']}",ha='center')
    
#     plt.axis('equal')
#     plt.axis("off")
# #%%
# def elastic_lpdictplot(lpdict,loi_id,bw,hgap=1,vgap=0.3,lvgap=0.05):
    
#     loi_node_list=lpdict[loi_id]['nodeList']
#     loi_link_list=list(zip(loi_node_list,loi_node_list[1:]))
    
#     loi_node_coords={loi_node_list[i]:hgap*i for i in range(len(loi_node_list))}
    
#     """Plot the LOI."""
#     plt.figure()
#     for i,j in loi_link_list:
#         plt.plot([loi_node_coords[i],loi_node_coords[j]],[0,0],'ko-')
    
#     """Plot the neighbor lightpaths."""
#     for demid in lpdict:
        
#         if demid==loi_id:
#             continue
        
#         ind=(lpdict[demid]['nu']-lpdict[loi_id]['nu'])/bw
#         lpnodes=lpdict[demid]['nodeList']
        
#         lpnodes_copy=lpnodes.copy()
        
#         lplinks_in_common=list(filter(lambda link: link in loi_link_list,list(zip(lpnodes_copy,lpnodes_copy[1:]))))
        
#         if len(lplinks_in_common)>0:
#             plt.plot([loi_node_coords[lplinks_in_common[0][0]],loi_node_coords[lplinks_in_common[-1][-1]]],[ind*vgap]*2,'o-')
#             plt.text(sum([loi_node_coords[lplinks_in_common[0][0]],loi_node_coords[lplinks_in_common[-1][-1]]])/2,ind*vgap+lvgap,demid,ha='center')
#             # plt.text(sum([loi_node_coords[lplinks_in_common[0][0]],loi_node_coords[lplinks_in_common[-1][-1]]])/2,ind*vgap-2.5*lvgap,f"slots=[{lpdict[demid]['minOccupiedSlotIndex']},{lpdict[demid]['maxOccupiedSlotIndex']}]",ha='center')
            
#             # plt.plot([loi_node_coords[lpnodes[0]],loi_node_coords[lpnodes[-1]]],[ind*vgap]*2,'o-')
#             # plt.text(sum([loi_node_coords[lpnodes[0]],loi_node_coords[lpnodes[-1]]])/2,ind*vgap+lvgap,demid,ha='center')
#             # plt.text(sum([loi_node_coords[lpnodes[0]],loi_node_coords[lpnodes[-1]]])/2,ind*vgap-2.5*lvgap,f"nu={lpdict[demid]['nu']/lpdict[demid]['chBandwidth']}",ha='center')
    
#     plt.axis('equal')
#     plt.axis("off")
#%%
def NormalizedRootRaisedCosine(t,beta):
    
    '''
    beta is roll-off factor.
    
    This pulse is normalized (symbol rate = 1).
    '''
    
    t=array(t)
    
    arg1=pi*t*(1-beta)
    arg2=pi*t*(1+beta)
    
    denom=pi*t*(1-(4*beta*t)**2)
    
    output=(sin(arg1)+4*beta*t*cos(arg2))/denom
    
    if not beta==0:
        output[t==1/4/beta]=beta/2**0.5*((1+2/pi)*sin(pi/4/beta)+(1-2/pi)*cos(pi/4/beta))
        output[t==-1/4/beta]=beta/2**0.5*((1+2/pi)*sin(pi/4/beta)+(1-2/pi)*cos(pi/4/beta))
    
    output[t==0]=1+beta*(4/pi-1)
    
    if any(isnan(output)):
        raise Exception('Error in RRC pulse. Output contains NaNs!')
    
    return output
#%%
def preProcessing(lpdict,loi_id,n_sym,rrc_beta,upsratio_elevation,Fs_type):
    
    """Extracting all modulations information"""
    all_modulation_formats=jsonload(open("Modulation_Alphabets_4D.json","r"))
    
    '''Pre-processing for lightpaths'''
    modulation_alphabet=all_modulation_formats[lpdict[loi_id]['modulationType']]
    modulation_size=len(modulation_alphabet)
    modulation_power=sum(norm(modulation_alphabet,2,axis=1)**2)/modulation_size
    
    LOI_TxSyms_at_unit_power=\
    array(modulation_alphabet)[randint(0,modulation_size,int(n_sym))].T/modulation_power**0.5
        
    loi_node_list=lpdict[loi_id]['nodeList']
    loi_link_list=list(zip(loi_node_list,loi_node_list[1:]))
    loi_sr=lpdict[loi_id]['symbolRate']
    
    '''Pulse shape'''
    t_rrc=linspace(-MU,MU,2*MU+1)/2
    rx_pulse_shape=NormalizedRootRaisedCosine(t_rrc,rrc_beta)
    
    '''
    Pre-processing at links:
        In this block, the four quantities of Fs, spectrum, upsampling ratio
        and minimum frequency at spectrum as well as span specs for all the links
        of LOI are calculated.
        
        Fsampling should fulfill 3 conditions:
            It must be an even multiple of LOI symbol rate.
            It must span all the occupied slots of the link spectrm.
            It must be at least twice as much as the largest symbol rate on the spectrum.
    '''
    fsampling_dict={}
    link_spectrum_dict={}
    link_lpid_dict={}
    upsratio_dict={}
    f_min_dict={}
    f_mid_dict={}
    loi_signal_length_dict={}
    time_D_dict={}
    freq_D_dict={}
    drop_filter_at_link_end_dict={}
    
    link_dropped_lpid_set_dict_FOR_DEBUGGING={}
    if Fs_type=='total':
        maximal_spectrum=set()
        for lp in lpdict.values():
            maximal_spectrum.add((lp['nu'],lp['chBandwidth']))
    elif not Fs_type=='per-link':
        raise Exception('Known Fs types are *per-link* and *total*')
        
    for i,j in loi_link_list:
        
        '''link_spectrum'''
        link_spectrum_dict[i,j]=set()
        link_lpid_dict[i,j]=set()
        for lpid,lp in lpdict.items():
            lp_node_list=lp['nodeList']
            lp_link_list=list(zip(lp_node_list,lp_node_list[1:]))
            if (i,j) in lp_link_list:
                link_spectrum_dict[i,j].add((lp['nu'],lp['chBandwidth']))
                link_lpid_dict[i,j].add(lpid)
        
        '''link_lpid'''
        if Fs_type=='total':
            link_spectrum_dict[i,j]=maximal_spectrum
        
        max_nu,max_nu_bw=max(link_spectrum_dict[i,j])
        min_nu,min_nu_bw=min(link_spectrum_dict[i,j])
        temp_fsampling=max(
            max_nu-min_nu+max_nu_bw/2+min_nu_bw/2,
            2*max({lpdict[lpid]["symbolRate"] for lpid_set in link_lpid_dict.values() for lpid in lpid_set})
            ) # Conditions 2 and 3
        upsratio=ceil(temp_fsampling/2/loi_sr)+upsratio_elevation
        fsampling=upsratio*2*loi_sr
        f_min=min_nu-min_nu_bw/2
        f_mid=(min_nu+max_nu)/2+(max_nu_bw-min_nu_bw)/4
        loi_signal_length=int((2*n_sym+len(rx_pulse_shape)-1)*upsratio)
        
        '''upsratio'''
        upsratio_dict[i,j]=upsratio
        
        '''fsampling'''
        fsampling_dict[i,j]=fsampling
        
        '''f_min'''
        f_min_dict[i,j]=f_min
        
        '''f_min'''
        f_mid_dict[i,j]=f_mid
        
        '''loi_signal_length'''
        loi_signal_length_dict[i,j]=loi_signal_length
        
        '''time_D'''
        time_D_dict[i,j]=arange(loi_signal_length)/fsampling
        
        '''freq_D'''
        freq_D_temp=arange(loi_signal_length)/loi_signal_length
        freq_D_dict[i,j]=(freq_D_temp-floor(freq_D_temp+0.5))*fsampling
    
    '''drop_filter_at_link_end'''
    for i,j in loi_link_list:
        
        freq_D=freq_D_dict[i,j]
        f_mid=f_mid_dict[i,j]
        total_drop_filter=zeros(loi_signal_length_dict[i,j])
        
        '''next node'''
        link_index=loi_link_list.index((i,j))
        if link_index==len(loi_link_list)-1:
            break
        else:
            _,k=loi_link_list[link_index+1]
        
        link_dropped_lpid_set=set(link_lpid_dict[i,j])-set(link_lpid_dict[j,k])
        link_dropped_lpid_set_dict_FOR_DEBUGGING[i,j]=link_dropped_lpid_set
        
        for lpid in link_dropped_lpid_set:
            lp_nu=lpdict[lpid]['nu']
            lp_bw=lpdict[lpid]['chBandwidth']
            lp_drop_filter=(abs(freq_D+f_mid-lp_nu)<=lp_bw/2)+0
            total_drop_filter+=lp_drop_filter
        
        drop_filter_at_link_end_dict[i,j]=1-total_drop_filter
    
    return LOI_TxSyms_at_unit_power,loi_node_list,loi_link_list,loi_sr,fsampling_dict,\
        link_lpid_dict,upsratio_dict,f_min_dict,f_mid_dict,loi_signal_length_dict,time_D_dict,freq_D_dict,rx_pulse_shape,\
        drop_filter_at_link_end_dict
#%%
# def TxOutput(lp_nu,lp_tx_syms,lp_upsratio,time_D,f_mid,rx_pulse_shape):
def TxOutput(lp_nu,lp_mod_type,lp_tx_syms,lp_upsratio,lp_dnsratio,time_D,f_mid,rx_pulse_shape,loi_signal_length):
    
    if len(lp_tx_syms)==0:
        
        all_modulation_formats=jsonload(open("Modulation_Alphabets_4D.json","r"))
        modulation_alphabet=all_modulation_formats[lp_mod_type]
        modulation_size=len(modulation_alphabet)
        modulation_power=sum(norm(modulation_alphabet,2,axis=1)**2)/modulation_size
        
        lp_n_sym=int(ceil((loi_signal_length*lp_dnsratio/lp_upsratio+1-len(rx_pulse_shape))/2))
        lp_tx_syms=array(modulation_alphabet)[randint(0,modulation_size,int(lp_n_sym))].T/modulation_power**0.5
    
    tx_syms_res=kron([[1,1j,0,0],[0,0,1,1j]]@lp_tx_syms,[1,0])
    
    baseband=array([convolve(tx_syms_res[0],rx_pulse_shape),convolve(tx_syms_res[1],rx_pulse_shape)])
    
    baseband_res=resample_poly(baseband,up=lp_upsratio,down=lp_dnsratio,axis=1)
    
    baseband_res=array([baseband_res[0][:loi_signal_length],baseband_res[1][:loi_signal_length]])
    
    return baseband_res*exp(2j*pi*(lp_nu-f_mid)*time_D)
#%%
def AddedSignalsForUnitPower(lpdict,loi_id,LOI_TxSyms_at_unit_power,loi_link_list,
    fsampling_dict,time_D_dict,freq_D_dict,f_mid_dict,upsratio_dict,loi_signal_length_dict,
    link_lpid_dict,rx_pulse_shape,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
    ampgain_dict,nf_dict,numstep_dict,slotBandwidth_GHz):
    
    '''Set of added Lps'''
    added_lps_dict={}
    prev_lpid_set=set()
    for i,j in loi_link_list:
        added_lps_dict[i,j]=link_lpid_dict[i,j]-prev_lpid_set
        prev_lpid_set=link_lpid_dict[i,j].copy()
    
    '''Added signals for unit power'''
    added_signal_at_unit_power_dict={}
    
    # raise Exception("asj,dnsakdhksajhdjakshdakjsdhkajs")
    
    for i,j in loi_link_list:
        
        '''Signal added to the beginning of link (i,j)'''
        temp_total_added_signal=zeros([2,loi_signal_length_dict[i,j]],dtype=npcomplex128)
        
        '''Sampling-related quantities'''
        fsampling = fsampling_dict [i,j]
        time_D    = time_D_dict    [i,j]
        freq_D    = freq_D_dict    [i,j]
        f_mid     = f_mid_dict     [i,j]
        upsratio  = upsratio_dict  [i,j]
        
        # raise Exception("asj,dnsakdhksajhdjakshdakjsdhkajs")
        
        for lpid in added_lps_dict[i,j]:
            
            '''Establishing set of lightpath links'''
            lp_out_of_bus_node_list=lpdict[lpid]['nodeList'][:1+lpdict[lpid]['nodeList'].index(i)]
            lp_out_of_bus_link_list=list(zip(lp_out_of_bus_node_list,lp_out_of_bus_node_list[1:]))
            
            """For the LOI, inject the pre-generated symbols. Otherwise, pass and empty list."""
            if lpid==loi_id:
                lp_tx_syms=LOI_TxSyms_at_unit_power
            else:
                lp_tx_syms=[]
            
            '''Creating lightpath signal'''
            temp_added_lp_signal=TxOutput(
                lpdict[lpid]['nu'],
                lpdict[lpid]["modulationType"],
                lp_tx_syms,
                upsratio*lpdict[loi_id]['symbolRate']/slotBandwidth_GHz,
                lpdict[lpid]['symbolRate']/slotBandwidth_GHz,
                time_D,
                f_mid,
                rx_pulse_shape,
                loi_signal_length_dict[i,j])
            
            # raise Exception("asj,dnsakdhksajhdjakshdakjsdhkajs")
            
            '''Propagating Lp signal through its links'''
            for p,q in lp_out_of_bus_link_list:
                temp_added_lp_signal,_,__=LinkPropagation(temp_added_lp_signal,freq_D,time_D,
                        fsampling,f_mid,alpha_dict[p,q],beta2_dict[p,q],gamma_dict[p,q],
                        lspan_dict[p,q],ampgain_dict[p,q],nf_dict[p,q],numstep_dict[p,q],"None","None","")
                
                
                # LinkPropagation(propagatedSignal,freq_D,time_D,fsampling,f_mid,alpha_list,
                #                 beta2_list,
                #                     gamma_list,lspan_list,ampgain_list,nf_list,
                #                     numstep_list,progressbar)
            
            '''Adding the output signal to total added signals'''
            temp_total_added_signal+=temp_added_lp_signal
        
        added_signal_at_unit_power_dict[i,j]=temp_total_added_signal
        # print(f'Total added signal created for link ({i},{j}).')
    
    '''DEBUGGING'''
    added_lps_dict_FOR_DEBUGGING=added_lps_dict.copy()
    
    return added_signal_at_unit_power_dict,added_lps_dict_FOR_DEBUGGING
#%%
def LinkPropagation(propagatedSignal,freq_D,time_D,fsampling,f_mid,alpha_list,beta2_list,
                    gamma_list,lspan_list,ampgain_list,nf_list,numstep_list,progressbar,
                    current_progress="None",total_progress="None",result_dir=""):
    
    h_planck=6.626e-34
    nu_ase=3e8/1550e-9
    
    link_nspan=len(alpha_list)
    
    for n in range(link_nspan):
        
        alpha=alpha_list[n]
        beta2=beta2_list[n]
        gamma=gamma_list[n]
        # lspan=lspan_list[n]
        ampgain=ampgain_list[n]
        # nf=nf_list[n]
        numstep=numstep_list[n]
        lenstep=lspan_list[n]/numstep_list[n]
        
        if alpha==0:
            lenstep_eff=lenstep
        else:
            lenstep_eff=(1-exp(-alpha*lenstep))/alpha
        
        full_exp=exp(-alpha*lenstep/2+2j*pi**2*beta2*lenstep*(freq_D+f_mid)**2)
        half_exp=exp(-alpha*lenstep/4+1j*pi**2*beta2*lenstep*(freq_D+f_mid)**2)
        propagatedSignal=ifft(fft(propagatedSignal)*half_exp)
        
        for _ in range(numstep):
            propagatedSignal*=exp(1j*8/9*gamma*lenstep_eff*([1,1]@abs(propagatedSignal)**2))
            propagatedSignal=ifft(fft(propagatedSignal)*full_exp)
            # if not type(progressbar)==str:
            #     progressbar.update(1)
            # if not result_dir=="":
            #     current_progress+=1
            #     with open(result_dir+"/SSFM_progress_log.txt","a") as file:
            #         file.writelines(f"{current_progress} of {total_progress} completed.\n")
        
        propagatedSignal=ifft(fft(propagatedSignal)/half_exp)*ampgain**0.5
        ase_var=h_planck*nu_ase*ampgain*nf_list[n]*fsampling
        ase_noise=[[1,1j,0,0],[0,0,1,1j]]@randn(4,len(propagatedSignal[0]))*(ase_var/4)**0.5*exp(-2j*pi*f_mid*time_D)
        propagatedSignal+=ase_noise
        
        if not result_dir=="":
            current_progress+=1
            with open(result_dir+"/SSFM_progress_log.txt","a") as file:
                file.writelines(f"{current_progress} of {total_progress} completed.\n")
    
    # if type(progressbar)==str:
    #     return propagatedSignal
    # else:
    return propagatedSignal,progressbar,current_progress
#%%
def RxDetection(rx_input,lp,freq_D,time_D,fsampling,f_mid,upsratio,acc_dispersion,rx_pulse_shape,n_sym):
    
    loi_signal_length=len(rx_input[0])
    
    lp_nu=lp['nu']
    lp_bw=lp['chBandwidth']
    
    '''EDC'''
    edc_out=ifft(fft(rx_input.copy())*exp(-2j*pi**2*acc_dispersion*(freq_D+f_mid)**2))
    
    '''to_baseband'''
    bb_out=edc_out*exp(-2j*pi*(lp_nu-f_mid)*time_D)
    
    '''LPF'''
    half_num_ones=int(ceil(lp_bw/fsampling*loi_signal_length/2))
    lpf=[1]*half_num_ones+[0]*(loi_signal_length-2*half_num_ones)+[1]*half_num_ones
    lpf_output=ifft(fft(bb_out)*lpf)
    
#            '''matched filter'''
#            t_rrc_res=np.linspace(-MU*upsratio,MU*upsratio,2*MU*upsratio+1)/2/upsratio
#            rx_pulse_shape_res=RootRaisedCosine(t_rrc_res,0.02)
#            mf_output=np.array([
#                    np.convolve(lpf_output[0],rx_pulse_shape_res),
#                    np.convolve(lpf_output[1],rx_pulse_shape_res)
#                    ])
#            '''downsample'''
#            ds_output=resample_poly(mf_output,up=1,down=upsratio,axis=1)
#            '''detection'''
#            rx_syms=np.array([
#                    ds_output[0][2*MU:2*MU+2*n_sym:2],
#                    ds_output[1][2*MU:2*MU+2*n_sym:2]
#                    ])
    
    '''downsample'''
    ds_output=resample_poly(lpf_output,up=1,down=upsratio,axis=1)
    
    '''matched filter'''
    mf_output=array([
            convolve(ds_output[0],rx_pulse_shape),
            convolve(ds_output[1],rx_pulse_shape)
            ])
    
    '''detection'''
    rx_syms=array([
            mf_output[0][2*MU:2*MU+2*int(n_sym):2],
            mf_output[1][2*MU:2*MU+2*int(n_sym):2]
            ])
    
    return rx_syms
#%%
def SNRCalculation(raw_rx_syms,raw_tx_syms,n_sym,sym_del_margin_ratio):
    
    sym_del_margin_length=max(int(n_sym*sym_del_margin_ratio/2),1)
    
    tx_syms=array([
            raw_tx_syms[0][sym_del_margin_length:-sym_del_margin_length],
            raw_tx_syms[1][sym_del_margin_length:-sym_del_margin_length],
            raw_tx_syms[2][sym_del_margin_length:-sym_del_margin_length],
            raw_tx_syms[3][sym_del_margin_length:-sym_del_margin_length],
            ])
    
    rx_syms=array([
            real(raw_rx_syms[0][sym_del_margin_length:-sym_del_margin_length]),
            imag(raw_rx_syms[0][sym_del_margin_length:-sym_del_margin_length]),
            real(raw_rx_syms[1][sym_del_margin_length:-sym_del_margin_length]),
            imag(raw_rx_syms[1][sym_del_margin_length:-sym_del_margin_length]),
            ])
    
    # print("s__________1")
    
    # import numpy as np
    
    coeff_mat=(rx_syms@tx_syms.T.conj())@inv(tx_syms@tx_syms.T.conj())
    
    # print("s__________2")
    
    noise=rx_syms-coeff_mat@tx_syms
    
    # print("s__________3")
    
    return tx_syms,rx_syms,coeff_mat,noise,\
        10*log10(trace(rx_syms@rx_syms.T.conj())/trace(noise@noise.T.conj())-1)
#%%
def SNR(lpdict,loi_id,alpha_dict,beta2_dict,gamma_dict,lspan_dict,nspan_dict,
        ampgain_dict,nf_dict,numstep_dict,acc_dispersion,power_set_dbm,
        n_sym,rrc_beta,upsratio_elevation,sym_del_margin_ratio,Fs_type,
        slotBandwidth_GHz,progress_bar_position,is_progress_bar_enabled,result_dir):

    ssfm_snr_db_list=[]
    
    """Progress bar"""
    # if is_progress_bar_enabled==1:
    total_progress=sum([nspan_dict[i,j] for i,j in zip(lpdict[loi_id]["nodeList"],lpdict[loi_id]["nodeList"][1:])])*len(power_set_dbm)
    # total_progress=sum([sum(numstep_dict[i,j])\
    #             for i,j in zip(lpdict[loi_id]["nodeList"],
    #                 lpdict[loi_id]["nodeList"][1:])])*\
    #                 len(power_set_dbm)
    current_progress=0
    # else:
    #     totalprogressbar="None"
        
    # print(f"progressbar is {totalprogressbar}")
    
    """SSFM pre-processing"""
    LOI_TxSyms_at_unit_power,loi_node_list,loi_link_list,loi_sr,fsampling_dict,\
    link_lpid_dict,upsratio_dict,f_min_dict,f_mid_dict,loi_signal_length_dict,\
    time_D_dict,freq_D_dict,rx_pulse_shape,drop_filter_at_link_end_dict=\
    preProcessing(lpdict,loi_id,n_sym,rrc_beta,upsratio_elevation,Fs_type)
    
    # raise Exception("asj,dnsakdhksajhdjakshdakjsdhkajs")
    
    # return TxSyms_at_unit_power_dict,loi_node_list,loi_link_list,loi_sr,fsampling_dict,\
    # link_lpid_dict,upsratio_dict,f_min_dict,f_mid_dict,loi_signal_length_dict,\
    # time_D_dict,freq_D_dict,rx_pulse_shape,drop_filter_at_link_end_dict
    
    '''Added signals for unit power'''
    added_signal_at_unit_power_dict,added_lps_dict_FOR_DEBUGGING=\
    AddedSignalsForUnitPower(lpdict,loi_id,LOI_TxSyms_at_unit_power,loi_link_list,
    fsampling_dict,time_D_dict,freq_D_dict,f_mid_dict,upsratio_dict,loi_signal_length_dict,
    link_lpid_dict,rx_pulse_shape,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
    ampgain_dict,nf_dict,numstep_dict,slotBandwidth_GHz)
    
    # raise Exception("asj,dnsakdhksajhdjakshdakjsdhkajs")
    # return added_signal_at_unit_power_dict,added_lps_dict_FOR_DEBUGGING
    
    
    
    for power_dbm in power_set_dbm:
        
        power=10**(0.1*power_dbm-3)
        
        network_propagated_signal=0j
        
        ''' TX '''
        for i,j in loi_link_list:
            
            '''Next node index'''
            if not loi_link_list[-1]==(i,j):
                _,k=loi_link_list[loi_link_list.index((i,j))+1]
            else:
                k='rx'
            
            '''Sampling-related quantities'''
            time_D=time_D_dict[i,j]
            freq_D=freq_D_dict[i,j]
            fsampling=fsampling_dict[i,j]
            f_mid=f_mid_dict[i,j]
            
            # upsratio and next_upsratio are used only for variable fsampling.
            upsratio=upsratio_dict[i,j]
            if not k=='rx':
                next_upsratio=upsratio_dict[j,k]
            else:
                next_upsratio=upsratio_dict[loi_link_list[0]]
            
            '''Adding signals'''
            network_propagated_signal+=added_signal_at_unit_power_dict[i,j]*(power**0.5)
            
            '''Link propagation'''
            network_propagated_signal,totalprogressbar,current_progress=LinkPropagation(
                    network_propagated_signal,freq_D,time_D,fsampling,f_mid,
                    alpha_dict[i,j],beta2_dict[i,j],gamma_dict[i,j],
                    lspan_dict[i,j],ampgain_dict[i,j],nf_dict[i,j],
                    numstep_dict[i,j],"Numeric",current_progress,total_progress,result_dir)
            
            '''Dropping signals (except for the last link)'''
            if not loi_link_list.index((i,j))==len(loi_link_list)-1:
                network_propagated_signal=ifft(fft(network_propagated_signal)*\
                                      drop_filter_at_link_end_dict[i,j])
        
#            '''Resampling'''
#            '''<<< Unfinished for variable Fs >>>'''
#            network_propagated_signal=resample_poly(network_propagated_signal,
#                                        up=next_upsratio,down=upsratio,axis=1)
        
        ''' Rx '''
        last_loi_link=loi_link_list[-1]
        
        last_freq_D    = freq_D_dict    [last_loi_link]
        last_time_D    = time_D_dict    [last_loi_link]
        last_fsampling = fsampling_dict [last_loi_link]
        last_f_mid     = f_mid_dict     [last_loi_link]
        last_upsratio  = upsratio_dict  [last_loi_link]
        
        raw_loi_rx_syms=RxDetection(network_propagated_signal,lpdict[loi_id],
                            last_freq_D,last_time_D,last_fsampling,last_f_mid,
                            last_upsratio,acc_dispersion,rx_pulse_shape,n_sym)
        
        raw_loi_tx_syms=LOI_TxSyms_at_unit_power*power**0.5
        
        tx_syms,rx_syms,coeff,noise,temp_snr_db=\
        SNRCalculation(raw_loi_rx_syms,raw_loi_tx_syms,n_sym,sym_del_margin_ratio)
        
        ssfm_snr_db_list.append(temp_snr_db)
    
    return ssfm_snr_db_list
#%%
if __name__=="__main__":
    
    from TopologyAndTrafficManager import randomSimulation,setUpSpanPropertiesAccordingToTheLOI
    import matplotlib.pyplot as plt
    import numpy as np
    
    args={'power_range_db': [0],
      'rrc_beta': 0.02,
      'nuMCp': 100,
      'numstep': 1,
      'n_sym': 7000.0,
      'upsratio_elevation': 2,
      'sym_del_margin_ratio': 0.8571428571428571,
      'Fs_type': 'total',
      'PT_TM_DB_dir': 'Backbone_Data',
      'PT_title': 'Test1_PT',
      'TM_title': 'Test1_TM',
      'loi_id': 'dem47',
      'modulationType': 'cube4_16',
      'nuOffset': 190000000000000.0,
      'slotBandwidth_GHz': 6.25,
      'numOfGuardSlotsPerDemand': 1,
      'span_approx_length_km': 125,
      'ampgain': "FC",
      'fiber_type': 'SMF',
      'nf': 3.548133892335755,
      'results_parent_dir': 'Results_Set2',
      'progress_bar_position': 0,
      'is_progress_bar_enabled': 1}

    
    power_range_db           = args["power_range_db"]
    rrc_beta                 = args["rrc_beta"]
    nuMCp                    = args["nuMCp"]
    numstep                  = args["numstep"]
    n_sym                    = args["n_sym"]
    upsratio_elevation       = args["upsratio_elevation"]
    sym_del_margin_ratio     = args["sym_del_margin_ratio"]
    Fs_type                  = args["Fs_type"]
    PT_TM_DB_dir             = args["PT_TM_DB_dir"]
    PT_title                 = args["PT_title"]
    TM_title                 = args["TM_title"]
    loi_id                   = args["loi_id"]
    modulationType           = args["modulationType"]
    nuOffset                 = args["nuOffset"]
    slotBandwidth_GHz        = args["slotBandwidth_GHz"]
    numOfGuardSlotsPerDemand = args["numOfGuardSlotsPerDemand"]
    span_approx_length_km    = args["span_approx_length_km"]
    ampgain                  = args["ampgain"]
    fiber_type               = args["fiber_type"]
    nf                       = args["nf"]
    results_parent_dir       = args["results_parent_dir"]
    progress_bar_position    = args["progress_bar_position"]
    is_progress_bar_enabled  = args["is_progress_bar_enabled"]
    
    # Apply RWA and determine the lightpaths.
    lpdict,lspan_dict,nspan_dict=randomSimulation(PT_title=PT_title,PT_DB_dir=PT_TM_DB_dir,TM_title=TM_title,
            TM_DB_dir=PT_TM_DB_dir,nuOffset=nuOffset,slotBandwidth_GHz=slotBandwidth_GHz,
            numOfGuardSlotsPerDemand=numOfGuardSlotsPerDemand,span_approx_length_km=span_approx_length_km,
            modulationType=modulationType,_type_="predefined-network")
    
    alpha_dict,beta2_dict,gamma_dict,nf_dict,ampgain_dict,numstep_dict,acc_dispersion=\
    setUpSpanPropertiesAccordingToTheLOI(lpdict,loi_id,fiber_type,nf,ampgain,
                                          numstep,lspan_dict,nspan_dict)
    
    # # Remoooooooooove this
    # lpdict={loi_id:lpdict[loi_id]}
    
    results={}
    
    ssfm_snr_db_list=[]
    
    power_set_dbm=[0]
    # """Progress bar"""
    # if is_progress_bar_enabled==1:
    #     totalprogressbar=tqdm(total=sum([sum(numstep_dict[i,j])\
    #                 for i,j in zip(lpdict[loi_id]["nodeList"],
    #                     lpdict[loi_id]["nodeList"][1:])])*\
    #                     len(power_set_dbm),position=progress_bar_position,leave=False)
    # else:
    #     totalprogressbar="None"
        
    # print(f"progressbar is {totalprogressbar}")
    
    """SSFM pre-processing"""
    LOI_TxSyms_at_unit_power,loi_node_list,loi_link_list,loi_sr,fsampling_dict,\
    link_lpid_dict,upsratio_dict,f_min_dict,f_mid_dict,loi_signal_length_dict,\
    time_D_dict,freq_D_dict,rx_pulse_shape,drop_filter_at_link_end_dict=\
    preProcessing(lpdict,loi_id,n_sym,rrc_beta,upsratio_elevation,Fs_type)
    
    # raise Exception("asj,dnsakdhksajhdjakshdakjsdhkajs")
    
    # return TxSyms_at_unit_power_dict,loi_node_list,loi_link_list,loi_sr,fsampling_dict,\
    # link_lpid_dict,upsratio_dict,f_min_dict,f_mid_dict,loi_signal_length_dict,\
    # time_D_dict,freq_D_dict,rx_pulse_shape,drop_filter_at_link_end_dict
    
    '''Added signals for unit power'''
    added_signal_at_unit_power_dict,added_lps_dict_FOR_DEBUGGING=\
    AddedSignalsForUnitPower(lpdict,loi_id,LOI_TxSyms_at_unit_power,loi_link_list,
    fsampling_dict,time_D_dict,freq_D_dict,f_mid_dict,upsratio_dict,loi_signal_length_dict,
    link_lpid_dict,rx_pulse_shape,alpha_dict,beta2_dict,gamma_dict,lspan_dict,
    ampgain_dict,nf_dict,numstep_dict,slotBandwidth_GHz)
    
    # raise Exception("asj,dnsakdhksajhdjakshdakjsdhkajs")
    # return added_signal_at_unit_power_dict,added_lps_dict_FOR_DEBUGGING
    
    for power_dbm in power_set_dbm:
        
        power=10**(0.1*power_dbm-3)
        
        network_propagated_signal=0j
        
        ''' TX '''
        for i,j in loi_link_list:
            
            '''Next node index'''
            if not loi_link_list[-1]==(i,j):
                _,k=loi_link_list[loi_link_list.index((i,j))+1]
            else:
                k='rx'
            
            '''Sampling-related quantities'''
            time_D=time_D_dict[i,j]
            freq_D=freq_D_dict[i,j]
            fsampling=fsampling_dict[i,j]
            f_mid=f_mid_dict[i,j]
            
            # upsratio and next_upsratio are used only for variable fsampling.
            upsratio=upsratio_dict[i,j]
            if not k=='rx':
                next_upsratio=upsratio_dict[j,k]
            else:
                next_upsratio=upsratio_dict[loi_link_list[0]]
            
            '''Adding signals'''
            network_propagated_signal+=added_signal_at_unit_power_dict[i,j]*(power**0.5)
            
            '''Link propagation'''
            network_propagated_signal,totalprogressbar,_=LinkPropagation(
                    network_propagated_signal,freq_D,time_D,fsampling,f_mid,
                    alpha_dict[i,j],beta2_dict[i,j],gamma_dict[i,j],
                    lspan_dict[i,j],ampgain_dict[i,j],nf_dict[i,j],
                    numstep_dict[i,j],"None","None","")
            
            '''Dropping signals (except for the last link)'''
            if not loi_link_list.index((i,j))==len(loi_link_list)-1:
                network_propagated_signal=ifft(fft(network_propagated_signal)*\
                                      drop_filter_at_link_end_dict[i,j])
        
#            '''Resampling'''
#            '''<<< Unfinished for variable Fs >>>'''
#            network_propagated_signal=resample_poly(network_propagated_signal,
#                                        up=next_upsratio,down=upsratio,axis=1)
        
        ''' Rx '''
        last_loi_link=loi_link_list[-1]
        
        last_freq_D    = freq_D_dict    [last_loi_link]
        last_time_D    = time_D_dict    [last_loi_link]
        last_fsampling = fsampling_dict [last_loi_link]
        last_f_mid     = f_mid_dict     [last_loi_link]
        last_upsratio  = upsratio_dict  [last_loi_link]
        
        raw_lp_rx_syms=RxDetection(network_propagated_signal,lpdict[loi_id],
                            last_freq_D,last_time_D,last_fsampling,last_f_mid,
                            last_upsratio,acc_dispersion,rx_pulse_shape,n_sym)
        
        raw_lp_tx_syms=LOI_TxSyms_at_unit_power*power**0.5
        
        tx_syms,rx_syms,coeff,noise,temp_snr_db=\
        SNRCalculation(raw_lp_rx_syms,raw_lp_tx_syms,n_sym,sym_del_margin_ratio)
        
        ssfm_snr_db_list.append(temp_snr_db)
    
    
    #%%
    plt.figure()
    plt.plot(ssfm_snr_db_list,"o")
    
    plt.figure()
    u=fft(added_signal_at_unit_power_dict["VI","V"][0])
    plt.plot(freq_D_dict["VI","V"]+f_mid_dict["VI","V"],u/max(abs(u)))
    
    for lpid in link_lpid_dict["VI","V"]:
        lp_nu=lpdict[lpid]["nu"]
        lp_bw=lpdict[lpid]["chBandwidth"]
        temp=np.linspace(lp_nu-lp_bw/2,lp_nu+lp_bw/2,10)
        temp1=np.sin((temp-lp_nu)/lp_bw*pi+0.5*pi)
        plt.plot(temp,temp1)
    
    #%%
    # print([f"{lpdict[lpid]['minOccupiedSlotIndex']},{lpdict[lpid]['maxOccupiedSlotIndex']}" for lpid in link_lpid_dict["VI","V"]])
    
    # print([fsampling_dict["VI","V"]/lpdict[lpid]["symbolRate"] for lpid in link_lpid_dict["VI","V"]])
    
    print([(upsratio,lpdict[lpid]["symbolRate"]/slotBandwidth_GHz/1e9) for lpid in link_lpid_dict["VI","V"]])
    # result_dir=results_parent_dir+"/"+ctime().replace(':','.')+" #"+createRandomHash()
    
    #'minOccupiedSlotIndex': 15,
    # 'maxOccupiedSlotIndex': 16,